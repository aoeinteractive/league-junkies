source 'https://rubygems.org'

gem 'rails', '3.2.18'
gem 'nokogiri'
gem 'pg', '~> 0.18.1'
gem 'puma'

# ENV variables
gem "figaro"

# Authentication
gem 'devise'
gem 'devise_invitable'
gem 'devise-async'

# Show UTC in local time
gem 'local_time'

# Timezone Detect
gem 'jstz-rails'

# JS Cookie
gem 'jquery-cookie-rails'

# File uploading
gem 'carrierwave'
gem 'carrierwave_backgrounder', git: "git://github.com/lardawge/carrierwave_backgrounder.git"
gem 'mini_magick'
gem 'fog'
gem 'remotipart', '~> 1.2'

# Geocoding
gem 'geocoder'
gem 'rubyzip' # maxmind

# Background Jobs
gem 'daemons'
gem 'clockwork'
gem 'sidekiq', '~> 4.0'

# Sidekiq Web
gem 'sinatra', require: false
gem 'slim'

# Riot API
gem 'ruby-lol'

# Google Places Wrapper
gem 'google_places'

# Hashtag
gem 'simple_hashtag', git: "git://github.com/colewinans/simple_hashtag.git"

# Parse Twitter Mentions
gem 'twitter-text'
gem 'jquery-atwho-rails', "~> 0.3.3"

# Textarea resize
gem 'autosize-rails'

# Pagination
gem 'will_paginate'
gem 'will_paginate-bootstrap'

# Bootstrap Date / Time Pickers
gem 'bootstrap-datepicker-rails'
gem 'bootstrap-timepicker-rails-addon', '~> 0.5.1'

# Timeago helper
gem 'rails-timeago'

# Readmore js
gem 'readmorejs-rails'

# OEmbed and Open Graph Parser
gem 'ruby-oembed'
gem 'opengraph_parser'

# Auto linker
gem 'rails_autolink'

# Private Messaging / Notifcations
gem 'mailboxer'

# Friendly Urls
gem 'friendly_id'

# Follows 
gem "acts_as_follower", git: 'git://github.com/tcocca/acts_as_follower.git', branch: 'rails_3'

# Search
gem 'searchkick'
gem 'typhoeus'

# Activity Feed
gem 'public_activity'

# Mailgun
gem 'mailgun-ruby', require: 'mailgun'

# Mailchimp
gem 'gibbon'

# SVG stuff
gem 'inline_svg'

# Meta tags
gem 'meta-tags', require: 'meta_tags'

# Fancybox Popup
gem 'fancybox2-rails', '~> 0.2.8'

gem 'faker'

# Embedly
gem 'embedly'

# Redis Mutexes
gem 'redis-semaphore'

group :assets do
  gem 'sass-rails',   '~> 3.2.3'
  gem 'coffee-rails', '~> 3.2.1'
  gem 'uglifier', '>= 1.0.3'
  gem 'bootstrap-sass', '~> 3.3.5'
  gem 'therubyracer', :platforms => :ruby
  gem 'quiet_assets'
  gem 'asset_sync'
  gem 'turbo-sprockets-rails3'  
end

group :development do
  gem 'capistrano', '~> 3.2.0'
  gem 'capistrano-rails', '~> 1.1.0'
  gem 'capistrano-bundler'
  gem 'capistrano-rbenv', "~> 2.0" 
  gem 'capistrano3-puma'  
end

group :development, :test do
  gem 'rspec-rails'
  gem 'capybara'
  gem 'factory_girl_rails'
  gem 'database_cleaner'
  gem 'shoulda-matchers'
  gem 'guard'
  gem 'guard-livereload'
  gem 'guard-rspec' 
  gem 'pry'
end

gem 'jquery-rails'

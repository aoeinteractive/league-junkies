var ready;
ready = function() {
  var users = new Bloodhound({
    datumTokenizer: function(d) {
        console.log(d);
        return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
        url: "/search/users?query=%QUERY",
        wildcard: "%QUERY"
    }
  });

  var hashtags = new Bloodhound({
    datumTokenizer: function(d) {
      console.log(d);
      return Bloodhound.tokenizers.whitespace(d.name);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: "/search/hashtags?query=%QUERY",
      wildcard: "%QUERY"
    }
  });

  $('.nav-search-input').typeahead(null, {
    name: 'hashtags',
    displayKey: 'name',
    source: hashtags.ttAdapter(),
    templates: {
      suggestion: function(data) {
        return '<a href="/hashtags/' + data.name + '">' +
                '<div class="typeahead-option">' + 
                '<div class="typeahead-hashtag">#' +
                data.name +
                '</div></div></a>';
      }
    }
  },
  {    
    name: 'users',
    displayKey: 'name',
    source: users.ttAdapter(),
    templates: {
      suggestion: function(data) {
        return '<a href="/users/' + data.username + '">' + 
               '<div class="typeahead-option clearfix">' + 
               '<img class="typeahead-photo pull-left" src="' + 
                data.image_url + 
                '"/>' + 
                '<div class="typeahead-name">' + 
                data.name + ' ' +
                '<span>@' + 
                data.username +
                '</span></div>' +
                '<div class="typeahead-summoner">' +
                data.summoner_region + ' - ' + data.summoner_name +
                '</div></div></a>';
      }
    }
  });
}

$(document).ready(ready);
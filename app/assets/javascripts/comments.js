$(document).ready(function() {
  $(document).on('keydown', '.enter-submit', function(e) {
    var input = $(e.currentTarget);
    if (!$.trim(this.value) == "") {
      if (e.keyCode == 13 && !e.shiftKey && input.data('enableEnter')) {
        e.preventDefault();
        input.closest('form').submit();
      }
    }
  });

  $('.comment-body').readmore({
    lessLink: '<a href="#">See Less</a>',
    moreLink: '<a href="#">See More</a>',
    embedCSS: false,
    collapsedHeight:100
  });  
});

$(document).on('click', 'a.load-more-comments', function(e) {
  e.preventDefault();
  $(this).html("<div class='ajax-loader medium'></div>");
});

$(document).on('focus', '.atwho', function(event) {
  event.preventDefault();
  var input = $(this);
  input.data('enableEnter', true);

  input.atwho({
    at: "@",
    limit: 6,
    show_the_at: true,
    callbacks: {
      remote_filter: function(query, callback) {
        if (query.length < 1) {
          return false
        }
        $.getJSON("/mentions/users.json", {q: query}, function(data) {
          callback(data);
        });
      }
    }
  });

  input.atwho({
    at: "#",
    limit: 4,
    show_the_at: true,
    callbacks: {
      remote_filter: function(query, callback) {
        if (query.length < 1) {
          return false
        }
        $.getJSON("/hashtags.json", {q: query}, function(data) {
          callback(data);
        });
      }
    }
  });  

  input.on('matched.atwho', function(e) {
    input.data('enableEnter', false);
  });
  input.on('inserted.atwho', function(e) {
    setTimeout(function() {
      input.data('enableEnter', true);
    }, 100);
  });  

});
var dateOptions = {
  autoclose: true,
  todayHighlight: true,
  ignoreReadonly: true
}

var timeOptions = {
  template: false
}

$(document).on("click", ".end-time-add", function() {
  $(".end-time-fields").show();
  $("#event_end_date").datepicker(dateOptions);
  $(".end-time-add").hide();
  $("#event_end_time").timepicker(timeOptions);

  if ($("#event_end_date").val().length == 0) {
    $("#event_end_date").datepicker("setDate", new Date());
  }
  $("#event_end_time").val($("#event_start_time").val());
});

$(document).on("click", ".end-time-remove", function() {
  $(".end-time-fields").hide();
  $("#event_end_date").val('');
  $("#event_end_date").datepicker("clearDates");
  $("#event_end_time").val('');
  $(".end-time-add").show();
});

$("#eventModal").on("show.bs.modal", function(e) {
  $("#event_start_date").datepicker(dateOptions);
  $("#event_start_time").timepicker(timeOptions);

  if ($("#event_start_date").val().length == 0) {
    $("#event_start_date").datepicker("setDate", new Date());
  }  

  if ($("#event_end_date").val().length) {
    $(".end-time-fields").show();
    $("#event_end_date").datepicker(dateOptions);
    $(".end-time-add").hide();
    $("#event_end_time").timepicker(timeOptions);
  }  
});

$(document).ready(function(){ 
  $(document).on('change', '#event_image', function(e) {
    $(".banner-ajax-container").show();    
    $(this).parents('form').submit();
  });
  $('.event-description').readmore({
    lessLink: '<a href="#">See Less</a>',
    moreLink: '<a href="#">See More</a>',
    embedCSS: false,
    collapsedHeight:100
  });    
});
var locationComponents = {
  locality: 'long_name',
  administrative_area_level_1: 'long_name',
  country: 'long_name'
};

function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}

function createOrFindLocation() {
  var place = autocomplete.getPlace();
  var inputDataKey = [];
  var inputData = $("#location_select").data();

  for (var key in inputData) {
    inputDataKey.push([key, inputData[key]]);
  }

  if (place.address_components) {
    var lat = place.geometry.location.lat();
    var lng = place.geometry.location.lng();
    var formatted = place.formatted_address
    var addressValues = {};
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      $.each(locationComponents, function(type, label) {
        if (addressType === type) {
          var val = place.address_components[i][label];
          addressValues[type] = val;
        }
      });
    }

    $.ajax({ 
      type: 'POST',
      dataType: 'json',
      url: '/locations',
      data: {
        'latitude': lat, 
        'longitude': lng,
        'city': addressValues["locality"],
        'state': addressValues["administrative_area_level_1"],
        'country': addressValues["country"],
        'formatted_address': formatted,
        'locatable_type': inputDataKey[0][0],
        'locatable_id': inputDataKey[0][1]
      }
    });
  }
}
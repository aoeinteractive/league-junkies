$(document).ready(function() {
  var message_text_area = $(".conversation-reply textarea");
  var button = $(".conversation-reply input[type=submit]");
  button.prop('disabled', true);
  message_text_area.keyup(function() {
    button.prop('disabled', $.trim(this.value) == "" ? true : false);
  });

  var load_messages = true;
  $(".messages-dropdown").on("shown.bs.dropdown", function() {
    if (load_messages == true) {
      $.ajax({url: "/messages/dropdown"}).done(function() {
        if ($('.messages-menu .pagination').length) {
          var container = $(".messages-dropdown-container");
          container.scroll(function() {
            var url;
            url = $(".messages-menu .pagination .next a").attr('href');
            if (url && container[0].scrollTop + container[0].offsetHeight >= container[0].scrollHeight) {
              $(".messages-menu .pagination").html("<div class='ajax-loader medium'></div>");
              return $.getScript(url);
            }
          });
          return container.scroll();
        }
      });
      load_messages = false;
    }
  });
});
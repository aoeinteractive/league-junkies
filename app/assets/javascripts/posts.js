$(document).ready(function() {
  var status = $(".create-status textarea");
  var button = $(".create-status input[type=submit]");
  button.prop('disabled', true);
  status.keyup(function() {
    button.prop('disabled', $.trim(this.value) == "" ? true : false);
  });
  
  $('.post-body').readmore({
    lessLink: '<a href="#">See Less </a>',
    moreLink: '<a href="#">See More</a>',
    embedCSS: false,
    afterToggle: function(trigger, element, expanded) {
      if(!expanded) {
        $('html, body').animate( { scrollTop: element.offset().top }, {duration: 100 } );
      }
    }    
  });

  if($('.feed-pagination').length) {
    $(window).scroll(function() {
      var url;
      url = $('.feed-pagination .next a').attr('href');
      if (url && $(window).scrollTop() > $(document).height() - $(window).height() - 50) {
        $('.feed-pagination').html("<div class='ajax-loader medium'></div>");
        $('.feed-pagination').show();
        return $.getScript(url);
      }
    });
    return $(window).scroll();
  }
});
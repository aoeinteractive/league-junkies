require 'clockwork'
require File.expand_path('../config/boot', File.dirname(__FILE__))
require File.expand_path('../config/environment', File.dirname(__FILE__))
include Clockwork

def execute_rake(file,task)
  require 'rake'
  rake = Rake::Application.new
  Rake.application = rake
  Rake::Task.define_task(:environment)
  load "#{Rails.root}/lib/tasks/#{file}"
  rake[task].invoke
end


Clockwork.configure do |config|
  config[:logger] = Logger.new(File.join(Rails.root, 'log', 'dev-clockwork.log')) if Rails.env.development?
  config[:logger] = Logger.new(File.join(Rails.root, 'log', 'prod-clockwork.log')) if Rails.env.production?  
end

every(10.minutes, 'refresh.trending') do 
  RefreshTrendingHashtag.perform_async
end

every(4.hours, 'riotnews.fetch') do 
  execute_rake "riot_news.rake", "riot_news:fetch"
end

# every(1.day, 'database.backup', at: '00:30', tz: 'Pacific Time (US & Canada)') do
#   Clockwork.execute_rake "psql_db.rake", 'psql_db:backup'
# end

every(1.hour, 'summoners.fetch') do
  CheckSummonerRevision.perform_async
end

every(1.day, 'riot_api.update', at: "00:00", tz: "Pacific Time (US & Canada)") do
  CheckRiotVersion.perform_async
end

every(1.day, 'embedly.update', at: "02:00", tz: "Pacific Time (US & Canada)") do
  RefreshOEmbedCache.perform_async
end
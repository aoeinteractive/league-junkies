class ApplicationController < ActionController::Base
  protect_from_forgery
  around_filter :with_timezone
  before_filter :http_basic_auth

  def location
    if Rails.env.test? || Rails.env.development?
      @location ||= Geocoder.search("Jamaica, New York").first
    else
      user_signed_in? && current_user.profile.location.present? ? 
        @location ||= current_user.profile.location :
        @location ||= request.location
    end
  end

private
  def with_timezone
    timezone = Time.find_zone(cookies[:timezone])
    Time.use_zone(timezone) {yield}
  end

  def http_basic_auth
    authenticate_or_request_with_http_basic do |username,password|
      username == 'junkiez' && password == 'sw33t'
    end if Rails.env.staging?
  end  
end

class CommentsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_commentable

  def index
    @comments = @commentable.comments.page(params[:comments_page]).per_page(5)
    respond_to do |format|
      format.html
      format.js
    end
  end

  def create
    @comment = current_user.comments.new(params[:comment].merge(commentable: @commentable))
    if @comment.save
      @comment.create_activity :create, owner: current_user, recipient: @commentable.user if @commentable.class.base_class == Post
      @commentable.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def destroy
    @comment = @commentable.comments.find(params[:id])
    if @comment.destroy
      @commentable.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private

  def find_commentable
    klass = [Post, Photo].detect { |c| params["#{c.name.underscore}_id"]}
    @commentable = klass.find(params["#{klass.name.underscore}_id"])       
  end
end

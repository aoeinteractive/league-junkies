module FeedSidebar
  extend ActiveSupport::Concern

  included do 
    before_filter :get_sidebar_info
  end

  def get_sidebar_info
    set_summoner
    set_rank
    set_invites_count
    load_suggested_friends
    load_activities
    load_trending
  end

  def set_summoner
    @summoner = current_user.summoners.verified.first
  end

  def set_rank
    @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first if @summoner
  end

  def set_invites_count
    @invite_count = current_user.invitation_limit
  end

  def load_suggested_friends
    @suggested_friends = current_user.suggested_friends
  end

  def load_activities
    @activities = PublicActivity::Activity.order("id DESC")
                                          .where(owner_id: current_user.following_ids).limit(5)
  end

  def load_trending
    @hashtags = SimpleHashtag::Hashtag.trending.limit(5)
  end

end
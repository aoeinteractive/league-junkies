module Metatags
  extend ActiveSupport::Concern

  # description tag vs og description
  # image and description don't change for now
  # this concern expects whatever the controller name is to be an instance ex: @user

  OG_TAGS = [:title, :type, :url, :description, :image]
  KEYWORDS = [
              'League Junkies', 
              'League of Legends', 
              'League of Legends Social Network',
              'Gaming Social Network', 
              'LoL', 
              'LoL Social Network',
              'LoL Summoners',
              'Summoners'
             ]

  included do 
    append_before_filter :set_metatags
  end

  module ClassMethods
    def metatags(hash)
      @hash = hash
    end
  end

private
  def set_metatags
    object = self.instance_variable_get("@#{self.class.to_us.underscore.split('_').first.singularize}")
    hash = self.class.instance_variable_get(:@hash)

    if object
      new_hash = {open_graph: {}}
      hash.each do |tag, method|
        value = object.send(method) if object.respond_to?(method)

        if tag == :keywords
          value = Array(value) + KEYWORDS
        elsif tag == :canonical
          value = self.send(method, object)
        end

        new_hash[tag] = value
        tag = :url if tag == :canonical
        new_hash[:open_graph][tag] = value if OG_TAGS.include?(tag)
      end
      set_meta_tags new_hash
    end
  end

end
class DashboardController < ApplicationController
  include FeedSidebar
  before_filter :authenticate_user!
 
  def index
    @posts = current_user.feed.page(params[:main_pagination]).per_page(5)
    @new_post = StatusMessage.new
    @news = RiotFeed.limit(3)
 
    # ew, this is just placeholder as this suggestions feature isn't finalized or fleshed out at all
    team_emails = ["info@leaguejunkies.com", "kucharz.michal@gmail.com", "rwells@leaguejunkies.com", "s.singh510@gmail.com", "stereodeviant@gmail.com", "huffmuffin9@gmail.com"]
    @leaguejunkies_team = User.where(email: team_emails).reverse
  end
end

class FavoritesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_favoritable

  def index
  end

  def create
    @favorite = current_user.favorites.new(favoritable: @favoritable)
    if @favorite.save
      @favorite.create_activity :create, owner: current_user, recipient: @favoritable.user
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end    
  end

  def destroy
    @favorite = current_user.favorites.where(favoritable_id: @favoritable.id, favoritable_type: @favoritable.class.base_class.to_s).first
    if @favorite.destroy
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end    
  end

private
  def find_favoritable
    klass = [Post].detect { |c| params["#{c.name.underscore}_id"]}
    @favoritable = klass.find(params["#{klass.name.underscore}_id"]) 
  end
end

class FollowTeamController < ApplicationController
  before_filter :authenticate_user!

  # if user already follows everyone, don't show
  # maybe add an expiration instead?
  def hide
    cookies.signed[:hide_follow_team] = {
      value: true,
      expires: 1.week.from_now
    }
    respond_to do |format|
      format.html {redirect_to :back}
      format.js
    end
  end
end
class FollowsController < ApplicationController
  before_filter :authenticate_user!
  
  def create
    @user = User.find(params[:id])   
    if current_user.follow(@user)
      @follow = Follow.where(follower_id: current_user.id, 
                             follower_type: "User", 
                             followable_id: @user.id, 
                             followable_type: "User").first
      @follow.create_activity :create, owner: current_user, recipient: @user
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    if current_user.stop_following(@user)
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end 
    end   
  end

  def followers
    @user = User.find(params[:user_id])
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
    end    
    @followers = @user.followers
  end

  def following
    @user = User.find(params[:user_id])
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
    end      
    @following = @user.following_users
  end

  def friends
    @user = User.find(params[:user_id])
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
    end      
    @friends = @user.followers & @user.following_users
  end

end

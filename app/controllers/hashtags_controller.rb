class HashtagsController < ApplicationController
  include FeedSidebar
  before_filter :authenticate_user!

  def index
    respond_to do |format|
      format.html { redirect_to root_path }
      format.json { render json: SimpleHashtag::Hashtag.where('name like ?', "#{params[:q]}%").map {|h| {name: h.display} } }
    end
  end

  def show
    @hashtag = SimpleHashtag::Hashtag.find_by_name(params[:hashtag])
    if @hashtag
      @hashtagged = @hashtag.hashtaggables.sort_by(&:created_at).reverse 
      @hashtagged = @hashtagged.paginate(page: params[:page], per_page: 5)
    end
    @news = RiotFeed.limit(3) 
  end

end

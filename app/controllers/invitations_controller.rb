class InvitationsController < ApplicationController
  def create
    @invitation = Invitation.new(params[:invitation])
    if @invitation.save
      InvitationMailer.delay.request_invite(@invitation.id)
      flash[:notice] = "Thanks for signing up. We'll invite you as soon as we can!"
      redirect_to root_path
    else
      render action: "new"
    end
  end
end

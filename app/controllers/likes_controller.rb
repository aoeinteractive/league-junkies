class LikesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_likeable

  def index
  end

  def create
    @like = current_user.likes.new(likeable: @likeable)
    if @like.save
      @like.create_activity :create, owner: current_user, recipient: @likeable.user if @likeable.class.base_class == Post
      @likeable.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def destroy
    @like = current_user.likes.where(likeable_id: @likeable.id, likeable_type: @likeable.class.base_class.to_s).first
    if @like.destroy
      @likeable.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private
  def find_likeable
    klass = [Photo, Post, Comment].detect { |c| params["#{c.name.underscore}_id"]}
    @likeable = klass.find(params["#{klass.name.underscore}_id"])    
  end
end

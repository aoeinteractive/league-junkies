class LocationsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_locatable, only: [:create] 
  def create
    @location = Location.where(city: params[:locality],
                               state: params[:state],
                               country: params[:country],
                               latitude: params[:latitude],
                               longitude: params[:longitude],
                               formatted_address: params[:formatted_address]
                              ).first_or_create
    @locatable.location = @location
    @locatable.save!
    render nothing: true
    # pictures: are you the owner? event/user
    # posts: are you the owner?
    # profile: is it your profile?
    # do some ifs so people cant tamper with html data attrs
  end

private
  def find_locatable
    klass = params[:locatable_type].split('Id')[0].classify.constantize
    if klass == User
      @locatable = current_user.profile
    end
  end
end

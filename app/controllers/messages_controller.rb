class MessagesController < ApplicationController
  before_filter :authenticate_user!

  def new
    respond_to do |format|
      format.js
    end
  end

  def show
    @mailbox = current_user.mailbox
    @conversations = @mailbox.conversations
    @conversation = @conversations.find(params[:id])    
    @conversation.mark_as_read(current_user)
    @messages = @conversation.messages.order("created_at ASC")
  end

  def create
    @user = User.find(params[:user])
    @receipt = current_user.send_message(@user, params[:body], params[:subject])
    respond_to do |format|
      format.js
    end
  end
end

class NearbyController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_profiles, only: [:summoners, :posts]
  before_filter :find_events
  before_filter :find_rank

  def summoners
    @profiles = @profiles.page(params[:main_pagination]).per_page(8)
  end

  def events
    @events = @events.page(params[:main_pagination]).per_page(6)
  end

  def posts
    @user_ids = @profiles.map(&:user_id)
    @posts = Post.unattached.where(user_id: @user_ids).page(params[:main_pagination]).per_page(5)
  end

private
  def find_events
    @events = Event.joins(:place).upcoming.recent.near([location.latitude, location.longitude], 50)
    @popular_events = @events.popular.limit(4)
    @upcoming_events = Event.upcoming.recent.limit(4)
  end

  def find_profiles
    @profiles = Profile.joins(:location).near([location.latitude, location.longitude], 50)    
  end

  def find_rank
    @summoner = current_user.summoners.verified.first    
    @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first if @summoner    
  end
end

class NotificationsController < ApplicationController
  include FeedSidebar
  
  before_filter :authenticate_user!
  before_filter :find_notifications

  def index
    @notifications.unread.update_all(read: true) if @notifications.unread.present?
    @notifications = @notifications.page(params[:main_pagination]).per_page(17)   
  end

  def dropdown
    @notifications = @notifications.page(params[:notifications_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

  def paginate
    @notifications = @notifications.page(params[:notifications_page]).per_page(7)
    respond_to do |format|
      format.js
    end
  end

private
  def find_notifications
    @notifications = current_user.notifications.order("id DESC")
  end
end

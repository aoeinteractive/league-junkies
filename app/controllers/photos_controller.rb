class PhotosController < ApplicationController
  before_filter :authenticate_user!
  def index
    @user = User.find(params[:user_id])
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
    end    
    @photos = @user.photos
  end

  def show
    @photo = Photo.find(params[:id])
    @user = @photo.user
    @comments = @photo.comments.page(params[:comments_page]).per_page(5)
    render layout: false
  end

  def new
    @photo = Photo.new
    respond_to do |format|
      format.html {redirect_to root_path}
      format.js
    end    
  end

  def create
    @photo = current_user.photos.new(params[:photo])
    if @photo.save
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def destroy
    @photo = current_user.photos.find(params[:id])
    if @photo.destroy
      flash[:notice] = "Photo has successfully been deleted."
      redirect_to user_photos_path(current_user)
    end
  end
end

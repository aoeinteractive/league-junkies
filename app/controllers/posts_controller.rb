class PostsController < ApplicationController
  include FeedSidebar
  before_filter :authenticate_user!

  def show
    @post = Post.find(params[:id])
    @news = RiotFeed.limit(3)  
  end

  def update
    
  end

  def destroy
    @post = current_user.posts.find(params[:id])
    @post_id = @post.id
    if @post.destroy
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

  def embedly
    @url = params[:url]
    @embedly = OEmbedCache.find_or_create_by(@url)
    if @embedly 
      respond_to do |format|
        format.js
      end
    end
  end
end

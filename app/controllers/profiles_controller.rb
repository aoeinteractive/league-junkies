class ProfilesController < ApplicationController
  before_filter :authenticate_user!

  def edit
    @user = current_user
    @profile = @user.profile
  end

  def update
    @user = current_user
    @profile = @user.profile
    if @profile.update_attributes(params[:profile])
      respond_to do |format|
        format.js
      end
    end
  end

  def show
    @footer_page = true    
    @user = User.find(params[:user_id])
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
      @normal_stats = @summoner.summoner_stats.where(game_type: "Unranked", season: "SEASON2016").first
    end
    @profile = @user.profile
  end

  def favorites
    @user = User.find(params[:user_id])
    @favorites = @user.favorites.page(params[:main_pagination]).per_page(5)
  end

  def remove_avatar
    @user = current_user
    @profile = @user.profile
    @profile.remove_image!
    @profile.save
  end

  def update_avatar
    @user = current_user
    @profile = @user.profile
    if @profile.update_attributes(image: params[:profile][:image])
      respond_to do |format|
        format.js
      end
    end
  end

  def edit_header
    @user = current_user
    @profile = @user.profile    
  end

  def update_header
    @user = current_user
    @profile = @user.profile
    if @profile.update_attributes(snippet: params[:profile][:snippet])
      respond_to do |format|
        format.js
      end
    end    
  end

end

class RsvpsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_rsvpable

  def index
    @rsvps = @rsvpable.rsvps.where(status: params[:status]).page(params[:rsvps_pagination]).per_page(6)
    respond_to do |format|
      format.js
    end    
  end

  def create
    @rsvp = current_user.rsvps.new(rsvpable: @rsvpable, status: params[:rsvp][:status])
    if @rsvp.save
      @rsvp.create_activity :create, owner: current_user, recipient: @rsvpable.host
      @rsvpable.reload
      respond_to do |format|
        format.js
      end
    end
  end

  def update
    @rsvp = current_user.rsvps.find(params[:id])
    if @rsvp.update_attributes(params[:rsvp])
      @rsvpable.reload
      respond_to do |format|
        format.js
      end
    end
  end

  def destroy
    
  end

  def paginate
    @rsvp = @rsvpable.rsvps.where(status: params[:status]).page(params[:rsvps_pagination]).per_page(6)
    respond_to do |format|
      format.js
    end    
  end

private
  def find_rsvpable
    klass = [Event].detect {|c| params["#{c.name.underscore}_id"]}
    @rsvpable = klass.find(params["#{klass.name.underscore}_id"])      
  end
end
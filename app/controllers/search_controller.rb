class SearchController < ApplicationController
  include FeedSidebar
  before_filter :authenticate_user!

  def index
    @users = User.confirmed.search params[:q], autocomplete: true
    #@hashtags = SimpleHashtag::Hashtag.search params[:q], autocomplete: true
    @news = RiotFeed.limit(3)  
  end

  def users
    render json: User.confirmed.search(params[:query], autocomplete: true, limit: 10).map {|u| {name: u.full_name, username: u.username, image_url: u.profile.image? ? u.profile.image_url(:thumb_small) : ActionController::Base.helpers.asset_path("small-user.png"), summoner_name: u.verified_summoner? ? u.summoners.verified.first.name : "No Summoner", summoner_region: u.verified_summoner? ? u.summoners.verified.first.region.name : "No Region", id: u.id}}
  end

  def hashtags
    render json: SimpleHashtag::Hashtag.search(params[:query], autocomplete: true, limit: 10).map {|h| {name: h.display}}
  end

end
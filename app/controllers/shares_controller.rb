class SharesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_shareable
  def new
    @share = Share.new
    respond_to do |format|
      format.html {redirect_to root_path}
      format.js
    end
  end
  
  def create
    if @shareable.is_a? Share
      @absolute_root = @shareable.absolute_root
      @share = current_user.shares.new(params[:share].merge(shareable: @absolute_root))
    else
      @absolute_root = @shareable
      @share = current_user.shares.new(params[:share].merge(shareable: @absolute_root))
    end

    if @share.save
     @absolute_root.reload
      respond_to do |format|
        format.js
      end
    else
      render nothing: true, status: 422
    end
  end

  def destroy
    if @shareable.is_a? Share
      @absolute_root = @shareable.absolute_root
      @share = current_user.shares.where(shareable_id: @absolute_root.id, shareable_type: @absolute_root.class.base_class.to_s).first
    else
      @absolute_root = @shareable
      @share = current_user.shares.where(shareable_id: @absolute_root.id, shareable_type: @absolute_root.class.base_class.to_s).first
    end
    @current_user_shared_id = @share.id
    if @share.destroy
      @absolute_root.reload
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end
  end

private

  def find_shareable
    klass = [Post].detect { |c| params["#{c.name.underscore}_id"]}
    @shareable = klass.find(params["#{klass.name.underscore}_id"])       
  end  
end
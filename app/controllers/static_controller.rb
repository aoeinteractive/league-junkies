class StaticController < ApplicationController
  before_filter :set_footer_and_bg

  def index
    @user = User.new
    @user.build_profile
  end

  def about
  end  

  def contact
  end

  def privacy
  end

  def tos
  end

private
  def set_footer_and_bg
    @home_page = true
    @footer_page = true    
  end
end

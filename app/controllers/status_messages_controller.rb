class StatusMessagesController < ApplicationController
  before_filter :authenticate_user!
  before_filter :find_postable, only: [:create]

  def create
    @status_message = current_user.status_messages.new(params[:status_message].merge(postable: @postable ? @postable : nil))
    if @status_message.save
      respond_to do |format|
        format.html {redirect_to :back}
        format.js
      end
    end    
  end

  def update
    
  end

private
  def find_postable
    resource, id = URI(request.referrer).path.split('/')[1,2]
    klass = [Event].detect {|c| c.name.underscore.pluralize == resource}
    @postable = klass.find(id) if klass
  end
end
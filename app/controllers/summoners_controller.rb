class SummonersController < ApplicationController
  before_filter :authenticate_user!
  #make sure they cant do summoners/dyrus/verify
  def create
    @summoner = current_user.summoners.new(params[:summoner])
    if @summoner.save
      respond_to do |format|
        format.js
      end
    end
  end

  def update    
  end

  # make ajax and support multiple
  def destroy
    @summoner = current_user.summoners.find(params[:id])
    if @summoner.destroy
      redirect_to edit_user_registration_path
    end
  end

  # need to rewrite to work for those without javascript
  # put into background job
  def verify
    if @summoner = current_user.summoners.find(params[:summoner_id])
      begin
        client = Lol::Client.new ENV["riot_key"], {region: @summoner.region.code}
        patch_version = RiotApi.patch.version
        riot_summoner = client.summoner.by_name(@summoner.name).first
        masteries = client.summoner.masteries(riot_summoner.id).first.last
        names = masteries.map {|m| m.name}
        if names.include? @summoner.verification_token
          @summoner.riot_id = riot_summoner.id
          @summoner.level = riot_summoner.summoner_level
          @summoner.remote_icon_url = "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/profileicon/#{riot_summoner.profile_icon_id}.png" 
          @summoner.revision_date = riot_summoner.revision_date
          @summoner.verify!   
          @summoner.initial_history
          @message = "Summoner successfully verified."   
        else
          @message = "No mastery page with that verification token can be found. Add the token to a mastery page and then refresh to try again."
        end
      rescue Exception => e
        error_message = e.message.present? ? e.message : "Looks like your summoner name is incorrect. Please add a new summoner and refresh to try again."
        @message = error_message
      end

      respond_to do |format|
        format.js {@message}
      end
    end      
  end

  # find a way to not give users access to any id here
  # render status appropriately, status code 200, 500, etc
  # def verification_status
  #   @status = Sidekiq::Status::status(params[:job_id])
  #   @job_status = Sidekiq::Status::get(params[:job_id], :job_status)
  #   @status = @status.present? ? @status : "NotReturned"
  #   @job_status = @job_status.present? ? @job_status : "NotSet"
  #   @message = Sidekiq::Status::message(params[:job_id])
  #   render json: {status: @status.to_s, verification: @job_status.to_s, message: @message}
  # end
end
class Users::InvitationsController < Devise::InvitationsController

  def new
    @invite_count = current_user.invitation_limit
    respond_to do |format|
      format.js
    end
  end

  def create
    @invite_count = current_user.invitation_limit
    if @invitation = Invitation.find_by_email(params[:email])
      @invitation.invitation_sent_at = Time.now.utc
      @invitation.save!
    end

    @invite = User.new(email: params[:email])
    @invite.skip_invite_validation!

    @invite.invite!(current_user)

    respond_to do |format|
      format.js
    end
  end

  def edit
    super
  end

  def update
    super
  end
end
class UsersController < ApplicationController
  before_filter :authenticate_user!
  
  def index
  end
  
  def show
    @user = User.confirmed.find(params[:id].downcase)
    if @summoner = @user.summoners.verified.first
      @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first
      @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
      @most_played_champion = @summoner.champion_summoners.sort_by(&:total_played).reverse.first
      @matches = @summoner.matches.limit(5)
    end
    @new_post = StatusMessage.new
    @posts = @user.posts.unattached.page(params[:main_pagination]).per_page(5)
  end

  def edit
    @footer_page = true
    @user = current_user
    @new_summoner = Summoner.new(region_id: Region.find_by_code("na").id)
    @summoner = current_user.summoners.first
    if @summoner && @summoner.verified?
      @ranked_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first
      @normal_stats = @summoner.summoner_stats.where(game_type: "Unranked", season: "SEASON2016").first
    end
  end

  def update
    @user = current_user
    @new_summoner = Summoner.new(region_id: Region.find_by_code("na").id)
    @summoner = current_user.summoners.verified.first
    if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete(:password)
        params[:user].delete(:password_confirmation)
    end       
    if @user.update_with_password(params[:user])
      sign_in @user, bypass: true
      flash[:notice] = "Settings have been successfully saved."
      redirect_to root_path
    else
      render :edit
    end
  end

  def matches
    @user = User.confirmed.find(params[:user_id].downcase)
    @summoner = @user.summoners.verified.first
    @rank = @summoner.summoner_leagues.where(queue: "RANKED_SOLO_5x5").first if @summoner
    @rank_stats = @summoner.summoner_stats.where(game_type: "RankedSolo5x5", season: "SEASON2016").first if @summoner    
    @matches = @summoner.matches.page(params[:main_pagination]).per_page(10)
    @user = User.find(params[:user_id])
  end
end

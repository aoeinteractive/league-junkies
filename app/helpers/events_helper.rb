module EventsHelper
  def event_dates(event)
    if event.end.present?
      event.start.to_date == event.end.to_date ? local_time(event.start, "%A, %B %e") : local_time(event.start, "%B %e") + " - " + local_time(event.end, "%B %e")
    else
      local_time(event.start, "%A, %B %e")
    end
  end

  def event_times(event)
    if event.end.present?
      event.start.to_date == event.end.to_date ? local_time(event.start, "%l:%M %p") + " - " +  local_time(event.end, "%l:%M %p") : local_time(event.start, "%b %e at %l:%M %p") + " to " + local_time(event.end, "%b %e %l:%M %p")
    else
      local_time(event.start, "%l:%M %p")
    end
  end
end
module MatchesHelper
  def game_type(subtype)
    case subtype
    when "NONE"; "custom game"
    when "NORMAL"; "normal game"
    when "NORMAL_3x3"; "normal game"
    when "ODIN_UNRAKNED"; "dominion game"
    when "ARAM_UNRANKED_5x5"; "aram game"
    when "BOT"; "bot game"
    when "BOT_3x3"; "bot game"
    when "RANKED_SOLO_5x5"; "ranked game"
    when "RANKED_TEAM_3x3"; "team game"
    when "RANKED_TEAM_5x5"; "team game"
    when "ONEFORALL_5x5"; "one for all"
    when "FIRSTBLOOD_1x1"; "snowdown 1v1"
    when "FIRSTBLOOD_2x2"; "snowdown 2v2"
    when "SR_6x6"; "hexakill game"
    when "CAP_5x5"; "team builder"
    when "URF_BOT"; "urf mode"
    when "URF"; "urf mode"
    when "NIGHTMARE_BOT"; "bot game"
    when "ASCENSION"; "ascension game"
    when "HEXAKILL"; "hexakill game"
    when "KING_PORO"; "king poro game"
    when "COUNTER_PICK"; "nemesis game"
    when "SEIGE"; "seige game"
    end
  end

  def match_map(map_id)
    case map_id
    when 1; "Summoner's Rift"
    when 2; "Summoner's Rift"
    when 3; "The Proving Grounds"
    when 4; "Twisted Treeline"
    when 8; "The Crystal Scar"
    when 10; "Twisted Treeline"
    when 11; "Summoner's Rift"
    when 12; "Howling Abyss"
    end
  end
end

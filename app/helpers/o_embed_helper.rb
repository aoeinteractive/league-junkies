module OEmbedHelper
  def oembed_image_class(cache)
    unless cache.type == "video" || cache.type == "rich"
      if cache.type == "link" && cache.thumbnail_width < 400
        "sm-img "
      end
    end
  end
end
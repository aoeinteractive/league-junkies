module RsvpsHelper
  def rsvp_status(status)
    case status
    when "yes"; "Going"
    when "maybe"; "Maybe"
    when "no"; "Not Going"
    end
  end

  def activity_status(status)
    case status
    when "yes"; "is going"
    when "maybe"; "might be going"
    when "no"; "is not going"
    end
  end  
end
module SummonersHelper
  def season_rank_image(tier)
    case tier
    when "CHALLENGER"; asset_path "summoner/badges/challenger.png"
    when "MASTER"; asset_path "summoner/badges/master.png"
    when "DIAMOND"; asset_path "summoner/badges/diamond.png"
    when "PLATINUM"; asset_path "summoner/badges/platinum.png"
    when "GOLD"; asset_path "summoner/badges/gold.png"
    when "SILVER"; asset_path "summoner/badges/silver.png"
    when "BRONZE"; asset_path "summoner/badges/bronze.png"
    else; asset_path "summoner/badges/placeholder.png"
    end
  end
end

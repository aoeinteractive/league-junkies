module UsersHelper
  def user_show_page
    params[:controller] == 'users' && params[:action] == 'show'
  end
end

class CheckRiotVersion
  include Sidekiq::Worker  
  def perform
    version = $riot_na.static.versions.get.first
    patch = RiotApi.patch
    unless version == patch.version
      patch.version = version
      patch.save
      [Champion, Item, Spell].each do |model|
        model.update_from_patch
      end
    end
  end
end
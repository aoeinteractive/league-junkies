class CheckSummonerRevision
  include Sidekiq::Worker  
  def perform
    regions = Region.all
    regions.each do |region|
      summoners = Summoner.verified.where(region_id: region.id)
      if summoners.present? 
        patch_version = RiotApi.patch.version
        summoner_ids = summoners.pluck(:riot_id)
        summoner_ids = summoner_ids.each_slice(40).to_a
        client = Lol::Client.new ENV["riot_key"], {region: region.code}
        summoner_ids.each do |ids|
          riot_summoners = client.summoner.get(ids)  
          riot_summoners.each do |riot_summoner|
            summoner = Summoner.find_by_riot_id(riot_summoner.id)
            unless summoner.revision_date == riot_summoner.revision_date
              summoner.level = riot_summoner.summoner_level
              summoner.remote_icon_url = "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/profileicon/#{riot_summoner.profile_icon_id}.png"
              summoner.revision_date = riot_summoner.revision_date
              summoner.save
              FetchSummonerStats.perform_async(summoner.id)
              FetchSummonerMatches.perform_async(summoner.id)
              FetchSummonerLeagues.perform_async(summoner.id)
              FetchSummonerStatsRanked.perform_async(summoner.id)            
            end
          end  
        end
      end
    end
  end
end
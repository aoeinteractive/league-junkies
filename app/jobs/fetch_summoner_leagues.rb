class FetchSummonerLeagues
  include Sidekiq::Worker
  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code}
    begin
      leagues = client.league.get_entries(summoner.riot_id).first.last
      leagues.each do |league|
        if summoner_league = summoner.summoner_leagues.find_by_queue(league.queue)
          summoner_league.update_attributes(
            tier: league.tier,
            division: league.entries.first.division,
            name: league.name,
            points: league.entries.first.league_points,
            fresh_blood: league.entries.first.is_fresh_blood,
            hot_streak: league.entries.first.is_hot_streak,
            inactive: league.entries.first.is_inactive,
            veteran: league.entries.first.is_veteran,
            player_or_team_id: league.entries.first.player_or_team_id,
            player_or_team_name: league.entries.first.player_or_team_name          
          )
        else
          SummonerLeague.create(
            summoner_id: summoner.id,
            queue: league.queue,
            tier: league.tier,
            division: league.entries.first.division,
            name: league.name,
            points: league.entries.first.league_points,
            fresh_blood: league.entries.first.is_fresh_blood,
            hot_streak: league.entries.first.is_hot_streak,
            inactive: league.entries.first.is_inactive,
            veteran: league.entries.first.is_veteran,
            player_or_team_id: league.entries.first.player_or_team_id,
            player_or_team_name: league.entries.first.player_or_team_name          
          )
        end
      end
    rescue Lol::NotFound => e
      logger.warn "Summoner has no rank stats, will ignore: #{e}"  
    end
  end
end
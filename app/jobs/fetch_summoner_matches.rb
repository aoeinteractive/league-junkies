class FetchSummonerMatches
  include Sidekiq::Worker
  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code}  
    recent_matches = client.game.recent(summoner.riot_id)
    recent_matches.each do |match|
      match_record = Match.where(
        riot_id: match.game_id,
        riot_date: match.create_date,
        game_mode: match.game_mode,
        game_type: match.game_type,
        game_sub_type: match.sub_type,
        map_id: match.map_id,
        region_id: summoner.region.id
      ).first_or_create
      
      MatchSummoner.where(
        match_id: match_record.id,
        summoner_id: summoner.id,
        ip_earned: match.ip_earned,
        level: match.level,
        spell1_id: Spell.find_by_riot_id(match.spell1) ? Spell.find_by_riot_id(match.spell1).id : nil,
        spell2_id: Spell.find_by_riot_id(match.spell2) ? Spell.find_by_riot_id(match.spell2).id : nil,
        item0_id: match.stats.item0 ? Item.find_by_riot_id(match.stats.item0).id : nil,
        item1_id: match.stats.item1 ? Item.find_by_riot_id(match.stats.item1).id : nil,
        item2_id: match.stats.item2 ? Item.find_by_riot_id(match.stats.item2).id : nil,
        item3_id: match.stats.item3 ? Item.find_by_riot_id(match.stats.item3).id : nil,
        item4_id: match.stats.item4 ? Item.find_by_riot_id(match.stats.item4).id : nil,
        item5_id: match.stats.item5 ? Item.find_by_riot_id(match.stats.item5).id : nil,
        item6_id: match.stats.item6 ? Item.find_by_riot_id(match.stats.item6).id : nil,
        champion_id: Champion.find_by_riot_id(match.champion_id).id,
        map_side: match.stats.team,
        win: match.stats.win
      ).first_or_create

      MatchStat.where(
        match_id: match_record.id,
        summoner_id: summoner.id,
        level: match.stats.level,
        total_damage_to_champions: match.stats.total_damage_dealt_to_champions,
        gold_earned: match.stats.gold_earned,
        wards_placed: match.stats.ward_placed,
        wards_killed: match.stats.ward_killed,
        total_damage_taken: match.stats.total_damage_taken,
        true_damage_dealt_player: match.stats.true_damage_dealt_player,
        physical_damage_dealt_player: match.stats.physical_damage_dealt_player,
        magic_damage_dealt_player: match.stats.magic_damage_dealt_player,
        true_damage_to_champions: match.stats.true_damage_dealt_to_champions,
        magic_damage_to_champions: match.stats.magic_damage_dealt_to_champions,
        physical_damage_to_champions: match.stats.physical_damage_dealt_to_champions,
        magic_damage_taken: match.stats.magic_damage_taken,
        physical_damage_taken: match.stats.physical_damage_taken,
        true_damage_taken: match.stats.true_damage_taken,
        killing_sprees: match.stats.killing_sprees,
        largest_crit: match.stats.largest_critical_strike,
        total_units_healed: match.stats.total_units_healed,
        double_kills: match.stats.double_kills,
        triple_kills: match.stats.triple_kills,
        quadra_kills: match.stats.quadra_kills,
        penta_kills: match.stats.pentra_kills,
        own_neutral_minions_killed: match.stats.neutral_minions_killed_your_jungle,
        enemy_neutral_minions_killed: match.stats.neutral_minions_killed_enemy_jungle,
        turrets_killed: match.stats.turrets_killed,
        assists: match.stats.assists,
        deaths: match.stats.num_deaths,
        kills: match.stats.champions_killed,
        crowd_control_dealt: match.stats.total_time_crowd_control_dealt,
        largest_multi_kill: match.stats.largest_multi_kill,
        total_damage_dealt: match.stats.total_damage_dealt,
        largest_killing_spree: match.stats.largest_killing_spree,
        total_heal: match.stats.total_heal,
        minions_killed: match.stats.minions_killed,
        time_played: match.stats.time_played,
        gold_spent: match.stats.gold_spent,
        neutral_minions_killed: match.stats.neutral_minions_killed,
        barracks_killed: match.stats.barracks_killed,
        sight_wards_bought: match.stats.sight_wards_bought,
        vision_wards_bought: match.stats.vision_wards_bought
      ).first_or_create
    end
  end
end
class FetchSummonerStats
  include Sidekiq::Worker
  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code}
    stats = client.stats.summary(summoner.riot_id)
    stats.each do |stat|
      if summoner_stat = SummonerStat.where(summoner_id: summoner.id, game_type: stat.player_stat_summary_type, season: "SEASON2016").first
        unless summoner_stat.modify_date == stat.modify_date
          summoner_stat.update_attributes(
            wins: stat.wins,
            losses: stat.losses,
            minion_kills: stat.aggregated_stats.total_minion_kills,
            neutral_minion_kills: stat.aggregated_stats.total_neutral_minion_kills,
            turret_kills: stat.aggregated_stats.total_turrets_killed,
            champion_kills: stat.aggregated_stats.total_champion_kills,
            assists: stat.aggregated_stats.total_stats,
            modify_date: stat.modify_date
          )       
        end
      else
        SummonerStat.create(
          summoner_id: summoner.id,
          season: "SEASON2016",
          game_type: stat.player_stat_summary_type,
          wins: stat.wins,
          losses: stat.losses,
          minion_kills: stat.aggregated_stats.total_minion_kills,
          neutral_minion_kills: stat.aggregated_stats.total_neutral_minion_kills,
          turret_kills: stat.aggregated_stats.total_turrets_killed,
          champion_kills: stat.aggregated_stats.total_champion_kills,
          assists: stat.aggregated_stats.total_assists,
          modify_date: stat.modify_date          
        )
      end
    end
  end
end
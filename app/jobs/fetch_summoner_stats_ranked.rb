class FetchSummonerStatsRanked
  include Sidekiq::Worker
  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code}
    begin
        ranked_stats = client.stats.ranked(summoner.riot_id)
        if summoner_stat = SummonerStat.where(summoner_id: summoner.id, game_type: "RankedSolo5x5", season: "SEASON2016").first
          unless summoner_stat.modify_date == ranked_stats.modify_date
            ranked_stats.champions.each do |champion|
              unless champion.id == 0
                if summoner_champion = summoner.champion_summoners.find_by_champion_id(champion.id)
                  summoner_champion.update_attributes(
                    total_deaths: champion.stats.total_deaths_per_session,
                    total_played: champion.stats.total_sessions_played,
                    total_damage_taken: champion.stats.total_damage_taken,
                    total_quadra_kills: champion.stats.total_quadra_kills,
                    total_triple_kills: champion.stats.total_triple_kills,
                    total_minion_kills: champion.stats.total_minion_kills,
                    max_champions_killed: champion.stats.max_champions_killed,
                    total_double_kills: champion.stats.total_double_kills,
                    total_physical_damage_dealt: champion.stats.total_physical_damage_dealt,
                    champion_kills: champion.stats.total_champion_kills,
                    assists: champion.stats.total_assists,
                    most_champions_killed: champion.stats.most_champion_kills_per_session,
                    total_damage_dealt: champion.stats.total_damage_dealt,
                    total_first_blood: champion.stats.total_first_blood,
                    losses: champion.stats.total_sessions_lost,
                    wins: champion.stats.total_sessions_won,
                    percentage: (champion.stats.total_sessions_won.to_f / champion.stats.total_sessions_played),
                    total_magic_damage_dealt: champion.stats.total_magic_damage_dealt,
                    total_gold_earned: champion.stats.total_gold_earned,
                    total_penta_kills: champion.stats.total_penta_kills,
                    total_turrets_killed: champion.stats.total_turrets_killed,
                    most_spells_cast: champion.stats.most_spells_cast,
                    total_deaths: champion.stats.total_deaths_per_session,
                    total_unreal_kills: champion.stats.total_unreal_kills                
                  )
                else
                  ChampionSummoner.create(
                    champion_id: Champion.find_by_riot_id(champion.id).id,
                    summoner_id: summoner.id,
                    total_deaths: champion.stats.total_deaths_per_session,
                    total_played: champion.stats.total_sessions_played,
                    total_damage_taken: champion.stats.total_damage_taken,
                    total_quadra_kills: champion.stats.total_quadra_kills,
                    total_triple_kills: champion.stats.total_triple_kills,
                    total_minion_kills: champion.stats.total_minion_kills,
                    max_champions_killed: champion.stats.max_champions_killed,
                    total_double_kills: champion.stats.total_double_kills,
                    total_physical_damage_dealt: champion.stats.total_physical_damage_dealt,
                    champion_kills: champion.stats.total_champion_kills,
                    assists: champion.stats.total_assists,
                    most_champions_killed: champion.stats.most_champion_kills_per_session,
                    total_damage_dealt: champion.stats.total_damage_dealt,
                    total_first_blood: champion.stats.total_first_blood,
                    losses: champion.stats.total_sessions_lost,
                    wins: champion.stats.total_sessions_won,
                    percentage: (champion.stats.total_sessions_won.to_f / champion.stats.total_sessions_played),
                    total_magic_damage_dealt: champion.stats.total_magic_damage_dealt,
                    total_gold_earned: champion.stats.total_gold_earned,
                    total_penta_kills: champion.stats.total_penta_kills,
                    total_turrets_killed: champion.stats.total_turrets_killed,
                    most_spells_cast: champion.stats.most_spells_cast,
                    total_deaths: champion.stats.total_deaths_per_session,
                    total_unreal_kills: champion.stats.total_unreal_kills
                  )
                end
              end
            end
          end
        end
    rescue Lol::NotFound => e
      logger.warn "Summoner has no rank stats, will ignore: #{e}"  
    end        
  end
end
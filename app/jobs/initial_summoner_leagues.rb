class InitialSummonerLeagues
  include Sidekiq::Worker
  sidekiq_options queue: "verification", retry: 5

  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code} 
    begin 
      leagues = client.league.get_entries(summoner.riot_id).first.last
      leagues.each do |league|
        SummonerLeague.where(
          summoner_id: summoner.id,
          queue: league.queue,
          tier: league.tier,
          division: league.entries.first.division,
          name: league.name,
          points: league.entries.first.league_points,
          fresh_blood: league.entries.first.is_fresh_blood,
          hot_streak: league.entries.first.is_hot_streak,
          inactive: league.entries.first.is_inactive,
          veteran: league.entries.first.is_veteran,
          player_or_team_id: league.entries.first.player_or_team_id,
          player_or_team_name: league.entries.first.player_or_team_name
        ).first_or_create
      end  
    rescue Lol::NotFound => e
      logger.warn "Summoner has no rank stats, will ignore: #{e}"  
    end      
  end
end
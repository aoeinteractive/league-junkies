class InitialSummonerStats
  include Sidekiq::Worker
  sidekiq_options queue: "verification"

  def perform(summoner_id)
    summoner = Summoner.find(summoner_id)
    client = Lol::Client.new ENV["riot_key"], {region: summoner.region.code}
    stats = client.stats.summary(summoner.riot_id)
    stats.each do |stat|
      SummonerStat.where(
        summoner_id: summoner.id,
        season: "SEASON2016", # cant be hardcoded for future proof
        game_type: stat.player_stat_summary_type,
        wins: stat.wins,
        losses: stat.losses,
        minion_kills: stat.aggregated_stats.total_minion_kills,
        neutral_minion_kills: stat.aggregated_stats.total_neutral_minion_kills,
        turret_kills: stat.aggregated_stats.total_turrets_killed,
        champion_kills: stat.aggregated_stats.total_champion_kills,
        assists: stat.aggregated_stats.total_assists,
        modify_date: stat.modify_date
      ).first_or_create
    end    
  end
end
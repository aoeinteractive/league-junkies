class RefreshTrendingHashtag
  include Sidekiq::Worker  
  def perform
    SimpleHashtag::Hashtag.find_each do |hashtag|
      posts = hashtag.hashtaggings.where(hashtaggable_type: "Post")
      unless posts.present?
        hashtag.update_attribute(:posts_count, 0)
        hashtag.update_attribute(:last_post_id, 0)
        hashtag.update_attribute(:last_post_at, nil)
      else
        last_post = posts.last.hashtaggable
        if last_post.created_at >= hashtag.updated_at
          post_id = last_post.id
          post_at = last_post.created_at
          posts_count = posts.length #total or within X amount of time
          hashtag.update_attribute(:posts_count, posts_count)
          hashtag.update_attribute(:last_post_id, post_id)
          hashtag.update_attribute(:last_post_at, post_at)
        end
      end
    end
  end
end
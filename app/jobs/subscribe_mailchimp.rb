class SubscribeMailchimp
  include Sidekiq::Worker

  def perform(user_id)
    user = User.find(user_id)
    gb = Gibbon::API.new
    gb.lists.subscribe({
      id: ENV["mailchimp_list"], 
      email: {email: user.email},
      double_optin: false,
      merge_vars: {:FNAME => user.profile.first_name, :LNAME => user.profile.last_name}
    })
  end
end
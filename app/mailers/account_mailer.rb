class AccountMailer < Devise::Mailer
  include Devise::Controllers::UrlHelpers

  def confirmation_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/confirmation_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@leaguejunkies.com", {"first" => "League", "last" => "Junkies"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Confirm Your League Junkies Account"
    message.body_html html_output.to_str
    message.add_tag "registration"
    sending = MAILGUN.send_message ENV["mailgun_domain"], message
  end

  def reset_password_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/reset_password_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@leaguejunkies.com", {"first" => "League", "last" => "Junkies"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Your League Junkies Password"
    message.body_html html_output.to_str
    message.add_tag "forgot-password"
    sending = MAILGUN.send_message ENV["mailgun_domain"], message    
  end

  def unlock_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/unlock_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@leaguejunkies.com", {"first" => "League", "last" => "Junkies"}
    message.add_recipient :to, @user.email, {"first" => @user.profile.first_name, "last" => @user.profile.last_name}
    message.subject "Your League Junkies Account"
    message.body_html html_output.to_str
    message.add_tag "unlock-account"
    sending = MAILGUN.send_message ENV["mailgun_domain"], message    
  end

  def invitation_instructions(record, token, opts={})
    @user = record
    @token = token
    html_output = render_to_string template: "devise/mailer/invitation_instructions"
    message = Mailgun::MessageBuilder.new()
    message.from "no-reply@leaguejunkies.com", {"first" => "League", "last" => "Junkies"}
    message.add_recipient :to, @user.email
    message.subject "Confirm Your League Junkies Account"
    message.body_html html_output.to_str
    message.add_tag "devise-invite"
    sending = MAILGUN.send_message ENV["mailgun_domain"], message    
  end

end
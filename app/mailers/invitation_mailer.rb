class InvitationMailer < ActionMailer::Base

  def request_invite(invitation_id)
    invitation = Invitation.find(invitation_id)
    template_name = "League Junkies Invite Request"
    template_content = []
    message = {
      to: [{email: invitation.email}],
      subject: "#{invitation.first_name}, Thanks for Registering!",
      merge_vars: [
        {rcpt: invitation.email, vars: [{name: "FNAME", content: invitation.first_name}]}
      ],
      track_opens: true,
      track_clicks: true,
      tags: ["registration"],
      inline_css: true,
      auto_text: true
    }

    sending = MANDRILL.messages.send_template template_name, template_content, message
  end 
end

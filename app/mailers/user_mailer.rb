class UserMailer < ActionMailer::Base
  default from: "no-reply@leaguejunkies.com"
end

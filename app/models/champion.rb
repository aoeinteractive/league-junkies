class Champion < ActiveRecord::Base
  attr_accessible :loading_image, :icon_image, :lore, :name, :rating, :riot_id, 
                  :splash_image, :stats, :tags, :title, :remote_loading_image_url, 
                  :remote_splash_image_url, :remote_icon_image_url
  serialize :tags, Array
  serialize :stats, Hash
  serialize :rating, Hash
  mount_uploader :loading_image, ChampionUploader, mounted_on: :loading_image
  mount_uploader :splash_image, ChampionUploader, mounted_on: :splash_image
  mount_uploader :icon_image, ChampionUploader, mounted_on: :icon_image

  def self.update_from_patch
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.champion.get
    datas.each do |data|
      if champion = Champion.find_by_riot_id(data.id)
        champion.update_attributes(
          name: data.name,
          remote_loading_image_url: "http://ddragon.leagueoflegends.com/cdn/img/champion/loading/#{data.key}_0.jpg",
          remote_icon_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/champion/#{data.key}.png"
        )
      else
        Champion.create(
          riot_id: data.id,
          name: data.name,
          remote_loading_image_url: "http://ddragon.leagueoflegends.com/cdn/img/champion/loading/#{data.key}_0.jpg",
          remote_icon_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/champion/#{data.key}.png"
        )        
      end
    end
  end
end

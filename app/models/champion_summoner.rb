class ChampionSummoner < ActiveRecord::Base
  attr_accessible :champion_id, :summoner_id, :total_deaths, :total_played, :total_damage_taken, :total_quadra_kills, :total_triple_kills, :total_minion_kills, :max_champions_killed, :total_double_kills, :total_physical_damage_dealt, :champion_kills, :assists, :most_champions_killed, :total_damage_dealt, :total_first_blood, :losses, :wins, :total_magic_damage_dealt, :total_gold_earned, :total_penta_kills, :total_turrets_killed, :most_spells_cast, :total_unreal_kills, :percentage
  
  belongs_to :champion
  belongs_to :summoner
end

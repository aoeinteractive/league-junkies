class Comment < ActiveRecord::Base
  include Mentioner
  include PublicActivity::Common

  has_many :likes, as: :likeable
  has_many :notifications, as: :notifiable, dependent: :destroy
  has_one :photo, as: :photoable
  belongs_to :commentable, polymorphic: true, counter_cache: true
  belongs_to :user
  attr_accessible :body, :user, :commentable
  validates :body, presence: true, length: {:maximum => 65535}
  after_create :create_notification 
  after_destroy :delete_activities
  default_scope order('id DESC')

  def create_notification
    if commentable.class.base_class == Post
      notifications.create(
        user: commentable.user,
        notifier: user
      ) unless user == commentable.user    
    end
  end

  def delete_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: "Comment")
    activities.destroy_all
  end
end

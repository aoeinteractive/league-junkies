module HashtagDisplay
  extend ActiveSupport::Concern

  def parsed_hashtags
    parsed_hashtags = []
    array_of_hashtags_as_string = scan_for_hashtags(hashtaggable_content)
    array_of_hashtags_as_string.each do |s|
      parsed_hashtags << SimpleHashtag::Hashtag.find_or_create_by_name(s[1]) do |hashtag|
        hashtag.display = s[1]
      end
    end
    parsed_hashtags
  end

end
module Mentionable
  extend ActiveSupport::Concern
  included do
    has_many :mentions, dependent: :destroy
  end
end
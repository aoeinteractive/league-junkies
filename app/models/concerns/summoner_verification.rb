module SummonerVerification
  extend ActiveSupport::Concern
  include ActionView::Helpers::DateHelper

  included do 
    before_create :generate_verification_token, if: :verification_required?
    before_update :postpone_name_change_until_verification_and_regenerate_verification_token, if: :postpone_name_change?
  end

  def initialize(*args, &block)
    @bypass_verification_postpone = false
    super
  end  

  def verify!
    pending_any_verification do
      self.verification_token = nil
      self.verified_at = Time.now.utc

      saved =  if unconfirmed_name.present?
        skip_reverification!
        self.name = unconfirmed_name
        self.unconfirmed_name = nil
        save(validate: true)
      else
        save(validate: false)
      end

      after_verification if saved
      saved
    end
  end

  def verified?
    !!verified_at
  end

  def pending_verification?
    unconfirmed_name.present?
  end

protected
 
  def pending_any_verification
    if (!verified? || pending_verification?)
      yield
    else
      self.errors.add(:name, :already_confirmed)
      false
    end
  end 

  def generate_verification_token
    token = generate_token(self.class, :verification_token)
    self.verification_token = token
    self.verification_sent_at = Time.now.utc
  end

  def verification_required?
    !verified?
  end

  def postpone_name_change_until_verification_and_regenerate_verification_token
    self.unconfirmed_name = self.name
    self.name = self.name_was
    generate_verification_token
  end

  def postpone_name_change?
    postpone = name_changed? && !@bypass_verification_postpone && self.name.present?
    @bypass_verification_postpone = false
    postpone
  end

  def after_verification
  end

  def skip_reverification!
    @bypass_verification_postpone = true
  end  

  def generate_token(klass, column)
    loop do
      token = Devise.friendly_token
      break token unless klass.to_adapter.find_first({ column => token })
    end
  end  
end

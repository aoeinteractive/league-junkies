class Favorite < ActiveRecord::Base
  include PublicActivity::Common
    
  attr_accessible :favoritable, :user
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :user
  belongs_to :favoritable, polymorphic: true
  after_create :create_notification 
  after_destroy :delete_activities

  def create_notification
    notifications.create(
      user: favoritable.user,
      notifier: user
    ) unless user == favoritable.user    
  end

  def delete_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: "Favorite")
    activities.destroy_all
  end  
end

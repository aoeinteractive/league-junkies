class Follow < ActiveRecord::Base
  include PublicActivity::Common

  extend ActsAsFollower::FollowerLib
  extend ActsAsFollower::FollowScopes

  has_many :notifications, as: :notifiable, dependent: :destroy

  # NOTE: Follows belong to the "followable" interface, and also to followers
  belongs_to :followable, polymorphic: true
  belongs_to :follower,   polymorphic: true

  after_create :create_notification
  after_destroy :delete_activities

  def create_notification
    notifications.create(
      user: followable,
      notifier: follower
    )    
  end

  def block!
    self.update_attribute(:blocked, true)
  end

  def delete_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: "Follow")
    activities.destroy_all
  end  

end

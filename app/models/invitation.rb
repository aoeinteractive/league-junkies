class Invitation < ActiveRecord::Base
  attr_accessible :username, :email, :first_name, :last_name
  email_regex = /\A[^@]+@[^@]+\z/
  username_regex = /^([a-zA-Z](_?[a-zA-Z0-9]+)*_?|_([a-zA-Z0-9]+_?)*)$/i  
  name_regex = /\A[^0-9`!@#\$%\^&*+_=]+\z/
  
  validates :username, presence: true, 
                       uniqueness: {case_sensitive: false}, 
                       format: {with: username_regex, message: "should only contain letters or numbers"},
                       length: {within: 1..20},
                       exclusion: {in: User::BLACKLIST, message: "is reserved."} 
  validates :email, presence: true,
                    uniqueness: {case_sensitive: false},
                    format: {with: email_regex, message: "should be valid email address"}

  validates :first_name, presence: true, length: {maximum: 32}, format: {with: name_regex, message: "should be formatted correctly"}
  validates :last_name, presence: true, length: {maximum: 32}, format: {with: name_regex, message: "should be formatted correctly"}

  validate :email_already_registered, :username_already_registered

  scope :not_invited, -> {where(invitation_sent_at: nil)}

private

  def email_already_registered
    errors.add :email, "is already registered" if User.find_by_email(email)
  end

  def username_already_registered
    errors.add :username, "is already registered" if User.find_by_username(username)
  end

end

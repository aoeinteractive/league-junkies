class Item < ActiveRecord::Base
  attr_accessible :base, :cost, :depth, :description, :from, :group, :image, :into, :name, 
                  :purchasable, :riot_id, :sell, :tags, :remote_image_url
  serialize :tags, Array
  serialize :from, Array
  serialize :into, Array
  mount_uploader :image, ItemUploader, mounted_on: :image

  def self.update_from_patch
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.item.get(itemListData: 'all')
    datas.each do |data|
      if item = Item.find_by_riot_id(data.id)
        item.update_attributes(
          name: data.name,
          remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/item/#{data.image['full']}"
        )
      else
        Item.create(
          riot_id: data.id,
          name: data.name,
          remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/item/#{data.image['full']}"          
        )
      end
    end    
  end
end

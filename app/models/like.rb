class Like < ActiveRecord::Base
  include PublicActivity::Common

  attr_accessible :likeable, :user
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :user
  belongs_to :likeable, polymorphic: true, counter_cache: true
  after_create :create_notification
  after_destroy :delete_activities

  def create_notification
    if likeable.class.base_class == Post
      notifications.create(
        user: likeable.user,
        notifier: user
      ) unless user == likeable.user   
    end
  end

  def delete_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: "Like")
    activities.destroy_all
  end
end

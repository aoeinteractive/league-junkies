class Location < ActiveRecord::Base
  attr_accessible :formatted_address, :city, :state, :country, :longitude, :latitude
  geocoded_by :formatted_address

  has_many :profiles
  has_many :users, through: :profiles
  has_many :posts
  has_many :status_messages

end

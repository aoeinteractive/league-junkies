class Match < ActiveRecord::Base
  attr_accessible :riot_id, :game_mode, :game_type, :game_sub_type, :map_id, :region_id, :season, :riot_date
  has_many :match_summoners
  has_many :summoners, through: :match_summoners
  has_many :match_stats
  default_scope order('riot_date DESC')
end

class MatchStat < ActiveRecord::Base
  attr_accessible :match_id, :summoner_id, :level, :total_damage_to_champions, :gold_earned, :wards_placed, :wards_killed, :total_damage_taken, :true_damage_dealt_player, :physical_damage_dealt_player, :magic_damage_dealt_player, :true_damage_to_champions, :magic_damage_to_champions, :physical_damage_to_champions, :magic_damage_taken, :true_damage_taken, :physical_damage_taken, :killing_sprees, :largest_crit, :total_units_healed, :double_kills, :triple_kills, :quadra_kills, :penta_kills, :own_neutral_minions_killed, :enemy_neutral_minions_killed, :turrets_killed, :assists, :deaths, :kills, :crowd_control_dealt, :largest_multi_kill, :total_damage_dealt, :largest_killing_spree, :total_heal, :minions_killed, :time_played, :gold_spent, :neutral_minions_killed, :barracks_killed, :sight_wards_bought, :vision_wards_bought
  
  belongs_to :match
  belongs_to :summoner
  default_scope order('created_at DESC')
end

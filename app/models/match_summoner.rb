class MatchSummoner < ActiveRecord::Base
  attr_accessible :match_id, :summoner_id, :ip_earned, :level, :spell1_id, :spell2_id, :item0_id, :item1_id, :item2_id, :item3_item, :item4_id, :item5_id, :item6_id, :champion_id, :map_side, :win
  belongs_to :summoner
  belongs_to :match
  belongs_to :champion
  belongs_to :spell1, class_name: :Spell
  belongs_to :spell2, class_name: :Spell
  belongs_to :item0, class_name: :Item
  belongs_to :item1, class_name: :Item
  belongs_to :item2, class_name: :Item
  belongs_to :item3, class_name: :Item
  belongs_to :item4, class_name: :Item
  belongs_to :item5, class_name: :Item
  belongs_to :item6, class_name: :Item


  default_scope order('created_at DESC')
end

class Mention < ActiveRecord::Base
  attr_accessible :user, :mentioner
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :user
  belongs_to :mentioner, polymorphic: true
  after_create :create_notification 
  
  def create_notification
    if mentioner.class == Comment
      unless mentioner.commentable.class == Photo
        notifications.create(
          user: user,
          notifier: mentioner.user
        ) unless user == mentioner.user
      end
    end
  end  
end

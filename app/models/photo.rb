class Photo < ActiveRecord::Base
  attr_accessible :user, :photoable, :description, :image, :random_string, :width, :height
  has_many :comments, as: :commentable
  has_many :likes, as: :likeable
  belongs_to :user
  belongs_to :photoable, polymorphic: true
  mount_uploader :image, PhotoUploader, mounted_on: :image
  validates :image, presence: true
  default_scope order('id DESC')
end

class Place < ActiveRecord::Base
  extend FriendlyId
  attr_accessible :name, :formatted_address, :html_text, :image, 
                  :formatted_phone_number,
                  :rating, :website, :street_number, :route, :city, :state, :country, :postal_code, :longitude, :latitude,
                  :google_place_id, :random_string, :slug, :remote_image_url

  friendly_id :name, use: :slugged
  mount_uploader :image, EventUploader, mounted_on: :image    
  has_many :events

  geocoded_by :formatted_address
end

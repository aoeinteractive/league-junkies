class Profile < ActiveRecord::Base
  belongs_to :user
  belongs_to :location
  attr_accessible :about, :snippet, :birthday, :websites, :gender, :first_name, :last_name, :image, :user, :random_string, :location_attributes, :remote_image_url, :location

  reverse_geocoded_by "locations.latitude", "locations.longitude"

  mount_uploader :image, PhotoUploader, mounted_on: :image
  name_regex = /\A[^0-9`!@#\$%\^&*+_=]+\z/

  after_commit :reindex_user

  validates :first_name, presence: true, length: {maximum: 32}, format: {with: name_regex, message: "should be formatted correctly"}
  validates :last_name, presence: true, length: {maximum: 32}, format: {with: name_regex, message: "should be formatted correctly"}

  accepts_nested_attributes_for :location

  def reindex_user
    user.reindex
  end

end

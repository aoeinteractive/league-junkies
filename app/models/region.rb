class Region < ActiveRecord::Base
  attr_accessible :code, :name
  has_many :summoners
  has_many :riot_feeds
end

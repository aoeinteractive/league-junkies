class RiotApi < ActiveRecord::Base
  attr_accessible :name, :version

  def self.patch
    where(name: "patch").first
  end

  def self.original_items
    where(name: "old_items").first
  end
end

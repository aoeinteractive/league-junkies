class RiotFeed < ActiveRecord::Base
  attr_accessible :description, :domain, :full_url, :image, :region, :title, :random_string, :remote_image_url
  belongs_to :region
  mount_uploader :image, NewsUploader, mounted_on: :image
  default_scope order('id DESC')
end

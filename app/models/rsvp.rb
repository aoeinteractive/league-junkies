class Rsvp < ActiveRecord::Base
  include PublicActivity::Common

  STATUSES = ["yes", "maybe", "no"]

  attr_accessible :rsvpable, :user, :status
  has_many :notifications, as: :notifiable, dependent: :destroy
  belongs_to :rsvpable, polymorphic: true, counter_cache: true
  belongs_to :user

  validates :user_id, uniqueness: {scope: [:rsvpable_id, :rsvpable_type]}
  validates :status, inclusion: {in: Rsvp::STATUSES}

  after_create :create_notification
  after_save :update_custom_counters
  after_destroy :delete_activities

private

  def create_notification
    notifications.create(
      user: rsvpable.host,
      notifier: user
    ) unless user == rsvpable.host   
  end

  def update_custom_counters
    rsvpable.refresh_rsvp_counters
  end

  def delete_activities
    activities = PublicActivity::Activity.where(trackable_id: self.id, trackable_type: "Rsvp")
    activities.destroy_all
  end  
end

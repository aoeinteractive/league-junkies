class Spell < ActiveRecord::Base
  attr_accessible :description, :image, :level, :name, :riot_id, :remote_image_url
  mount_uploader :image, SpellUploader, mounted_on: :image

  def self.update_from_patch
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.summoner_spell.get(spellData: 'all')
    datas.each do |data|
      if spell = Spell.find_by_riot_id(data.id)
        spell.update_attributes(
          name: data.name,
          description: data.sanitizedDescription,
          level: data.summonerLevel,
          remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/spell/#{data.image['full']}"
        )
      else
        Spell.create(
          riot_id: data.id,
          name: data.name,
          description: data.sanitizedDescription,
          level: data.summonerLevel,
          remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/spell/#{data.image['full']}"
        )
      end
    end    
  end
end

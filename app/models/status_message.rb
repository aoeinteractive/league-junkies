class StatusMessage < Post
  has_one :photo, as: :photoable, dependent: :destroy

  after_commit :attach_oembed_data, on: :create, if: :oembed_url
  attr_accessible :oembed_url
  attr_accessor :oembed_url

private
  def attach_oembed_data
    self.o_embed_cache = OEmbedCache.find_by_url(oembed_url)
    self.save
  end
end
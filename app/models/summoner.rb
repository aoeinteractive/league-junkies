class Summoner < ActiveRecord::Base
  extend FriendlyId
  friendly_id :name, use: :slugged  
  belongs_to :region
  belongs_to :user
  attr_accessible :level, :name, :riot_id, :icon, :verification_token, :verified_at, :verification_sent_at, :unconfirmed_name, :role, :user_id, :region_id
  mount_uploader :icon, SummonerIconUploader, mounted_on: :icon
  after_create :reindex_user
  include SummonerVerification

  has_many :summoner_stats, dependent: :destroy
  has_many :champion_summoners, dependent: :destroy
  has_many :champions, through: :champion_summoners
  has_many :summoner_leagues, dependent: :destroy
  has_many :match_summoners, dependent: :destroy
  has_many :matches, through: :match_summoners
  has_many :match_stats, dependent: :destroy

  scope :verified, -> { where("verified_at IS NOT NULL") }
  
  def initial_history
    InitialSummonerStats.perform_async(id)
    InitialSummonerMatches.perform_async(id)
    InitialSummonerLeagues.perform_async(id)
    InitialSummonerStatsRanked.perform_async(id)
  end

  def reindex_user
    user.reindex
  end

end

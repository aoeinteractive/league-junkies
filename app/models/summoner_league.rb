class SummonerLeague < ActiveRecord::Base
  attr_accessible :summoner_id, :queue, :tier, :division, :name, :points, :fresh_blood, :hot_streak, :inactive, :veteran, :player_or_team_id, :player_or_team_name
  belongs_to :summoner
end

class SummonerStat < ActiveRecord::Base
  attr_accessible :summoner_id, :season, :game_type, :wins, :losses, :minion_kills, :neutral_minion_kills, :turret_kills, :champion_kills, :assists, :modify_date
  belongs_to :summoner
end

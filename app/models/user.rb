class User < ActiveRecord::Base
  include Mentionable
  extend FriendlyId
  searchkick autocomplete: ['username', 'full_name', 'summoner_name']
  acts_as_messageable
  acts_as_followable
  acts_as_follower
  friendly_id :username, use: :slugged  
  devise :database_authenticatable, :registerable, :rememberable,
         :recoverable, :trackable, :validatable, :confirmable, :lockable, :timeoutable, :invitable, :async

  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :slug, :profile_attributes

  BLACKLIST = %w(admin administrator administer)
  username_regex = /^([a-zA-Z](_?[a-zA-Z0-9]+)*_?|_([a-zA-Z0-9]+_?)*)$/i  
  validates :username, presence: true, 
                       uniqueness: {case_sensitive: false}, 
                       format: {with: username_regex, message: "should only contain letters or numbers"},
                       length: {within: 1..20},
                       exclusion: {in: User::BLACKLIST, message: "is reserved."}, unless: :is_an_invite?
  
  #has_many :albums, dependent: :destroy
  has_many :photos, dependent: :destroy
  #has_many :tags, dependent: :destroy
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :status_messages
  has_many :favorites, dependent: :destroy
  has_many :likes, dependent: :destroy
  has_many :invitations, class_name: self.to_s, as: :invited_by
  has_many :notifications, dependent: :destroy
  has_many :sent_notifications, as: :notifier, class_name: "Notification", dependent: :destroy
  has_many :events, as: :host, dependent: :destroy
  has_many :rsvps, dependent: :destroy
  has_many :rsvp_events, through: :rsvps, source: :rsvpable, source_type: "Event"
  has_one :profile, dependent: :destroy
  accepts_nested_attributes_for :profile
  has_many :summoners, dependent: :destroy

  has_many :shares, dependent: :destroy, class_name: "Share"
  # shares scope
  scope :confirmed, lambda { where("confirmed_at IS NOT NULL")}
  default_scope order('id DESC')

  after_create :subscribe_user_to_mailing_list
  after_invitation_accepted :subscribe_user_to_mailing_list

  def full_name
    "#{profile.first_name} #{profile.last_name}"
  end

  def feed
    ids = following_ids << id
    Post.unattached.where(user_id: ids)
  end

  def suggested_friends
    ids = following_ids << id
    User.confirmed.where("id not in (?)", ids)
  end

  def following_ids
    following_users.map{|s| s.id}
  end

  def mailboxer_email(object)
    email
  end

  def likes?(object_id, object_type)
    likes.exists?(likeable_id: object_id, likeable_type: object_type)
  end

  def favorites?(object_id, object_type)
    favorites.exists?(favoritable_id: object_id, favoritable_type: object_type)
  end

  def shares?(object_id, object_type)
    shares.exists?(shareable_id: object_id, shareable_type: object_type)
  end  

  def unverified_summoner
    summoners.where(verified_at: nil).first
  end

  def verified_summoner?
    summoners.where("verified_at IS NOT NULL").present?
  end

  def rsvp_to!(rsvpable, status)
    rsvps.create!(rsvpable: rsvpable, status: status)
  end

  def rsvped_to?(rsvpable_id, rsvpable_type)
    rsvps.where(rsvpable_id: rsvpable_id, rsvpable_type: rsvpable_type).exists?
  end

  def password_required?
    super if confirmed?
  end

  def password_match?
    self.errors[:password] << "can't be blank" if password.blank?
    self.errors[:password_confirmation] << "can't be blank" if password_confirmation.blank?
    self.errors[:password_confirmation] << "does not match password" if password != password_confirmation
    password == password_confirmation && !password.blank?
  end

  def send_reset_password_instructions
    super if invitation_token.nil?
  end

  def should_index?
    confirmed?
  end

  def search_data
    {
      username: username, 
      full_name: full_name,
      summoner_name: verified_summoner? ? summoners.verified.first.name : nil
    }
  end

  def is_an_invite?
    @bypass_validation_for_invite ||= false
  end

  def skip_invite_validation!
    @bypass_validation_for_invite = true
  end

private
  
  # def prevent_username_changes
     # need this to work with invited shit
     # if username_changed? && self.persisted?
     #   if username?
     #     errors.add(:username, "cannot be changed!")
     #   end
     # end
  # end

  def subscribe_user_to_mailing_list
    SubscribeMailchimp.perform_async(self.id) unless !invited_by.nil? && !invitation_token.nil?
  end

end

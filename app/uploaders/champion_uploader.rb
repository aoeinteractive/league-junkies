class ChampionUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  include ::CarrierWave::Backgrounder::Delay

  process :set_content_type

  def store_dir
    custom_mount = case mounted_as
                   when :loading_image; "loading"
                   when :splash_image; "splash"
                   when :icon_image; "icon"
                   end

    "riot/champions/#{custom_mount}"
  end

end

class EventUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  include ::CarrierWave::Backgrounder::Delay

  # need version size from fernando finish
  # version thumb
  # version sidebar
  # version full size
  process :set_content_type
  before :cache, :reset_secure_token

  version :thumb do
    process :resize_to_fill => [149,138]
  end

  version :sidebar do
    process :resize_to_fill => [280,114]
  end

  version :full do
    process :resize_to_fill => [1140,302]
  end

  def store_dir
    "images"
  end

  def filename
    "#{secure_token(10)}.#{file.extension}" if original_filename.present?
  end

  def extension_white_list
    %w(jpg jpeg png tiff)
  end

protected

  def secure_token(length)
    model.random_string ||= SecureRandom.hex(length)
  end

  def reset_secure_token(file)
    model.random_string = nil
  end    

end

class SpellUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  include ::CarrierWave::Backgrounder::Delay

  process :set_content_type

  def store_dir
    "riot/spells"
  end

end

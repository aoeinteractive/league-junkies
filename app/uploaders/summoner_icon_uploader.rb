class SummonerIconUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  include CarrierWave::MimeTypes
  include ::CarrierWave::Backgrounder::Delay

  process :set_content_type

  def store_dir
    "riot/icons"
  end

  version :thumb_small do
    process :resize_to_fill => [92,92]
  end

end

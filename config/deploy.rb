# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'leaguejunkies'
set :repo_url, 'git@bitbucket.org:aoeinteractive/league-junkies.git'
set :deploy_to, '/home/deployer/app/'
set :scm, :git
set :linked_files, %w{config/database.yml config/application.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}
set :pty, true
set :default_env, { path: "~/.rbenv/shims:~/.rbenv/bin:$PATH" }
set :keep_releases, 5
# setup rbenv.
set :rbenv_type, :user
set :rbenv_ruby, '2.1.5'
set :rbenv_map_bins, %w{rake gem bundle ruby rails}

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
      invoke 'clockwork:restart'
    end
  end

  after :publishing, :restart
  after :published, 'nginx:reload'
  #after :published, 'searchkick:reindex'

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end

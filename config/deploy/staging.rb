set :stage, :staging
set :rails_env, "staging"
set :app_env, "#{fetch(:application)}_#{fetch(:rails_env)}"
set :user, 'deployer'
set :branch, 'location'
set :puma_threads, [0,5]
set :puma_workers, 0
set :puma_bind, "unix://#{shared_path}/tmp/sockets/puma.sock"
set :puma_state, "#{shared_path}/tmp/pids/puma.state"
set :puma_pid, "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true
server '159.203.133.127', user: "#{fetch(:user)}", roles: %w{web app db}, primary: true
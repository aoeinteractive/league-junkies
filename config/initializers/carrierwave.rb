require 'fog/aws/storage'
require 'carrierwave'

CarrierWave.configure do |config|
  if Rails.env.development? || Rails.env.test?
    config.storage = :file
  else
    config.storage = :fog
    config.fog_credentials = {
      provider: "AWS",
      aws_access_key_id: ENV["aws_key"],
      aws_secret_access_key: ENV["aws_secret"],
      path_style: true
    }
    config.fog_directory = ENV["carrierwave_bucket"]
    config.fog_attributes = {'Cache-Control'=>'max-age=2678400'}
    config.asset_host = ENV["carrierwave_host"]
  end
end
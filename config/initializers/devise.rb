Devise.setup do |config|
  config.secret_key = 'ffa5628d6d90397d1e1f556ed28b24bc4c2d63627db0f1e033e08c1d4222b49aeb1ffa642c9d0095b7d553a8088d127ee5291e9df23d7bff21e7a514d73ecda0'
  config.mailer_sender = 'League Junkies <no-reply@leaguejunkies.com>'
  config.mailer = 'AccountMailer'
  require 'devise/orm/active_record'
  config.authentication_keys = [ :email ]
  # config.request_keys = []
  config.case_insensitive_keys = [ :email ]
  config.strip_whitespace_keys = [ :email ]
  # config.params_authenticatable = true
  # config.http_authenticatable = false
  # config.http_authenticatable_on_xhr = true
  # config.http_authentication_realm = 'Application'
  # config.paranoid = true
  config.skip_session_storage = [:http_auth]
  # config.clean_up_csrf_token_on_authentication = true
  config.stretches = Rails.env.test? ? 1 : 10
  # config.pepper = 'd1285854f5ae7ae74c969b821c40bc0c270e94899358370e2718f52e49613feea9317a6123eedb21aa0fcd12be52898ca180be9de18944e43c70e5ba5c9f1660'
  # config.allow_unconfirmed_access_for = 2.days
  # config.confirm_within = 3.days
  config.reconfirmable = true
  # config.confirmation_keys = [ :email ]
  config.remember_for = 7.days
  config.expire_all_remember_me_on_sign_out = true
  # config.extend_remember_period = false
  # config.rememberable_options = {}
  config.password_length = 8..128
  # config.email_regexp = /\A[^@]+@[^@]+\z/
  config.timeout_in = 12.hours
  # config.expire_auth_token_on_timeout = false
  # config.lock_strategy = :failed_attempts
  # config.unlock_keys = [ :email ]
  # config.unlock_strategy = :both
  # config.maximum_attempts = 20
  # config.unlock_in = 1.hour
  # config.last_attempt_warning = true
  # config.reset_password_keys = [ :email ]
  config.reset_password_within = 6.hours
  # config.encryptor = :sha512
  # config.scoped_views = false
  # config.default_scope = :user
  # config.sign_out_all_scopes = true
  # config.navigational_formats = ['*/*', :html]
  config.sign_out_via = :delete
  # config.omniauth :github, 'APP_ID', 'APP_SECRET', scope: 'user,public_repo'
  # config.warden do |manager|
  #   manager.intercept_401 = false
  #   manager.default_strategies(scope: :user).unshift :some_external_strategy
  # end
  # config.router_name = :my_engine
  # config.omniauth_path_prefix = '/my_engine/users/auth'
  # config.invite_for = 0
  config.invitation_limit = 10
  config.invite_key = {:email => /\A[^@]+@[^@]+\z/}
  config.validate_on_invite = true
  # config.resend_invitation = true
  config.invited_by_class_name = 'User'
  config.invited_by_counter_cache = :invitations_count
end

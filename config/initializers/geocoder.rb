Geocoder.configure(
  timeout: 30,
  lookup: :bing,
  bing: {
    api_key: ENV["bing_key"]
  },
  cache: Redis.new,
  ip_lookup: :maxmind_local,
  maxmind_local: {package: :city}
)

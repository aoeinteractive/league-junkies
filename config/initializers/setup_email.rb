ActionMailer::Base.smtp_settings = {
  :address        => "smtp.mailgun.org",
  :port           => 587,
  :user_name      => ENV["mailgun_username"],
  :password       => ENV["mailgun_password"],
}

MAILGUN = Mailgun::Client.new ENV["mailgun_key"]
RailsJunkies::Application.routes.draw do
  require 'sidekiq/web'

  get 'hashtags/',              to: 'hashtags#index', as: :hashtags
  get 'hashtags/:hashtag',      to: 'hashtags#show', as: :hashtag  
  get 'mentions/users',         to: 'mentions#users'
  get 'follow_team/hide',       to: 'follow_team#hide', as: :hide_follow_team
  get 'latest-updates',         to: 'activities#latest_updates', 
                                as: :latest_updates
  get 'about',                  to: 'static#about'
  get 'rsvps/paginate',         to: 'rsvps#paginate'
  get 'notifications/paginate', to: 'notifications#paginate'
  get 'messages/paginate',      to: 'conversations#paginate'
  get 'contact',                to: 'static#contact'
  get 'privacy',                to: 'static#privacy'
  get 'tos',                    to: 'static#tos'
  get 'search',                 to: 'search#index'
  get 'search/users',           to: 'search#users'
  get 'search/hashtags',        to: 'search#hashtags'
  get 'messages',               to: 'conversations#index'
  get 'notifications/dropdown', to: 'notifications#dropdown'
  get 'messages/dropdown',      to: 'conversations#dropdown'
  get 'profile/edit',           to: 'profiles#edit'
  get 'profile/edit/header',    to: 'profiles#edit_header'
  get 'nearby/events',          to: 'nearby#events'
  get 'nearby/summoners',       to: 'nearby#summoners'
  get 'nearby/posts',           to: 'nearby#posts'
  post 'embedly',               to: 'posts#embedly', as: 'embedly'
  resources :mentions, only: [:index]
  resources :notifications
  resources :activities, only: [:index]
  resources :events do
    resources :rsvps
    get 'hosting', on: :collection
    get 'rsvps', on: :collection
    put "update_image", to: 'events#update_image'
    delete "remove_image", to: 'events#remove_image'
  end
  #resources :invitations, only: [:create]
  resources :users, only: [:index, :show, :update] do
    resources :profiles
    get 'likes', to: 'users#likes'
    get 'favorites', to: 'profiles#favorites'
    get 'about', to: 'profiles#show'
    get 'photos', to: 'photos#index'
    get 'followers', to: 'follows#followers'
    get 'following', to: 'follows#following'
    get 'friends', to: 'follows#friends'
    get 'matches', to: 'users#matches'
    delete 'remove_avatar', to: 'profiles#remove_avatar'
    put 'update_avatar', to: 'profiles#update_avatar'
    put 'update_header', to: 'profiles#update_header'
  end
  resources :photos do
    resources :comments
    resources :likes, only: [:index, :create]
    delete 'likes', to: 'likes#destroy'
  end
  resources :champions
  resources :spells
  resources :items
  resources :status_messages, only: [:new, :create, :update]
  resources :messages, only: [:new, :create, :show]
  resources :conversations, only: [:index] do
    member do
      post :reply
    end
  end
  post 'follow/:id', to: 'follows#create', as: :follow
  delete 'follow/:id', to: 'follows#destroy', as: :follow
  resources :posts, only: [:show, :destroy] do
    resources :comments
    resources :favorites, only: [:index, :create]
    resources :likes, only: [:index, :create]
    resources :shares, only: [:index, :new, :create]
    delete 'likes', to: 'likes#destroy'
    delete 'favorites', to: 'favorites#destroy'
    delete 'shares', to: 'shares#destroy'
  end
  resources :locations
  resources :summoners do
    get 'verify', to: 'summoners#verify'
  end
  get 'summoner_status/:job_id', to: 'summoners#verification_status'
  devise_for :users, path: '', skip: [:registrations, :invitations], path_names: {sign_in: 'login', sign_out: 'logout', password: 'password-reset', confirmation: 'confirm-account'}, controllers: {confirmations: 'confirmations'}
  as :user do
    get 'register', to: 'users/registrations#new', as: 'new_user_registration'
    post 'register', to: 'devise/registrations#create', as: 'user_registration'
    get 'users/cancel', to: 'devise/registrations#cancel', as: 'cancel_user_registration'
    get 'settings', to: 'users#edit', as: 'edit_user_registration'
    put 'settings', to: 'users#update'
    get 'invite/accept', to: 'users/invitations#edit', as: 'accept_user_invitation'
    get 'invite/remove', to: 'devise/invitations#destroy', as: 'remove_user_invitation'
    post 'invite', to: 'users/invitations#create', as: "user_invitation"
    get 'invite/new', to: 'users/invitations#new', as: "new_user_invitation"
    put 'invite', to: 'users/invitations#update'
    put 'confirm', to: 'confirmations#confirm'
  end
  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == ENV["sidekiq_username"] && password == ENV["sidekiq_password"]
  end
  mount Sidekiq::Web => '/sidekiq'
  authenticated :user do 
    root to: "dashboard#index", as: "authenticated_root"
  end
  
  root to: "static#index"
end
class CreateSummoners < ActiveRecord::Migration
  def change
    create_table :summoners do |t|
      t.integer  "riot_id"
      t.string   "name"
      t.integer  "level"
      t.integer  "region_id"
      t.integer  "user_id"
      t.string   "verification_token"
      t.datetime "verification_sent_at"
      t.datetime "verified_at"
      t.string   "unconfirmed_name"
      t.timestamps
    end
  end
end

class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.string   "address"
      t.string   "state"
      t.string   "city"
      t.string   "country"
      t.float    "latitude"
      t.float    "longitude"
      t.integer  "locatable_id"
      t.string   "locatable_type"
      t.timestamps
    end
  end
end

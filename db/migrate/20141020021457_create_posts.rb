class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.text     "body"
      t.integer  "user_id"
      t.string   "postable_type"
      t.integer  "postable_id"
      t.timestamps
    end
  end
end

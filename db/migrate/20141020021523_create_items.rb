class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string   "riot_id"
      t.string   "name"
      t.text     "description"
      t.text     "tags"
      t.string   "image"
      t.text     "from"
      t.text     "into"
      t.integer  "depth"
      t.integer  "cost"
      t.integer  "sell"
      t.integer  "base"
      t.boolean  "purchasable"
      t.string   "group"
      t.timestamps
    end
  end
end

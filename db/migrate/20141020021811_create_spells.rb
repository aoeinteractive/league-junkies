class CreateSpells < ActiveRecord::Migration
  def change
    create_table :spells do |t|
      t.integer  "riot_id"
      t.string   "name"
      t.integer  "level"
      t.text     "description"
      t.string   "image"
      t.timestamps
    end
  end
end

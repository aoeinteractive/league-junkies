class CreateChampions < ActiveRecord::Migration
  def change
    create_table :champions do |t|
      t.integer  "riot_id"
      t.string   "name"
      t.text     "tags"
      t.text     "stats"
      t.string   "splash_image"
      t.string   "loading_image"
      t.string   "title"
      t.text     "lore"
      t.text     "rating"
      t.timestamps
    end
  end
end

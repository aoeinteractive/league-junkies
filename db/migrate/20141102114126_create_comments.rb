class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string   :commentable_type
      t.integer  :commentable_id
      t.belongs_to :user
      t.text :body

      t.timestamps
    end
    add_index :comments, [:commentable_id, :commentable_type]
    add_index :comments, :user_id
  end
end

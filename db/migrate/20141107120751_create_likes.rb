class CreateLikes < ActiveRecord::Migration
  def change
    create_table :likes do |t|
      t.string :liker_type
      t.integer :liker_id
      t.string :likeable_type
      t.integer :likeable_id

      t.timestamps
    end
    add_index :likes, [:liker_type, :liker_id]
    add_index :likes, [:likeable_type, :likeable_id]
  end
end

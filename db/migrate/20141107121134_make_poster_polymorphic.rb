class MakePosterPolymorphic < ActiveRecord::Migration
  def change
    remove_column :posts, :user_id
    add_column :posts, :poster_id, :integer
    add_column :posts, :poster_type, :string
    add_index :posts, [:poster_type, :poster_id]
  end
end

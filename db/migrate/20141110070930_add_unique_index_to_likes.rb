class AddUniqueIndexToLikes < ActiveRecord::Migration
  def change
    add_index :likes, [:likeable_type, :likeable_id, :liker_type, :liker_id], unique: true, name: "likes_unique_index"
  end
end

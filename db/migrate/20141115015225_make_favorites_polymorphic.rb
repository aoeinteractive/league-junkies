class MakeFavoritesPolymorphic < ActiveRecord::Migration
  def change
    rename_column :favorites, :user_id, :favoriter_id
    add_column :favorites, :favoriter_type, :string
  end
end

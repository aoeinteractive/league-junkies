class AddIndexToFavoriter < ActiveRecord::Migration
  def change
    add_index :favorites, [:favoriter_type, :favoriter_id]
  end
end

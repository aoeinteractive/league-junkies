class RemovePostableFromPost < ActiveRecord::Migration
  def change
    remove_column :posts, :postable_type
    remove_column :posts, :postable_id   
  end
end

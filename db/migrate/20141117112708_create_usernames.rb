class CreateUsernames < ActiveRecord::Migration
  def change
    create_table :usernames do |t|
      t.integer :owner_id
      t.string :owner_type
      t.string :name

      t.timestamps
    end
    add_index :usernames, [:owner_id, :owner_type], unique: true
    add_index :usernames, :name
  end
end

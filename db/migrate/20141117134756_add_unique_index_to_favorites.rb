class AddUniqueIndexToFavorites < ActiveRecord::Migration
  def change
    add_index :favorites, [:favoritable_type, :favoritable_id, :favoriter_type, :favoriter_id], unique: true, name: "favorites_unique_index"    
  end
end

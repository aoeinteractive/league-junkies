class AddDefaultCountToPost < ActiveRecord::Migration
  def change
    change_column :posts, :shares_count, :integer, default: 0
    change_column :posts, :likes_count, :integer, default: 0
    change_column :posts, :favorites_count, :integer, default: 0
    change_column :posts, :comments_count, :integer, default: 0    
  end
end

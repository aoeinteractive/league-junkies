class MakeCommentsPoly < ActiveRecord::Migration
  def change
    rename_column :comments, :user_id, :commenter_id
    add_column :comments, :commenter_type, :string
  end
end

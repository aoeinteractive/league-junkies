class AddUserIdToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :user_id, :integer
    remove_index :posts, [:poster_type, :poster_id]
    remove_column :posts, :poster_type
    rename_column :posts, :poster_id, :page_id
  end
end

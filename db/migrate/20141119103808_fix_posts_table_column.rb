class FixPostsTableColumn < ActiveRecord::Migration
  def change
    remove_column :posts, :page_id
    add_column :posts, :page_id, :integer
    add_index :posts, :page_id
    add_index :posts, :user_id
  end
end

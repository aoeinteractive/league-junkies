class AddUserToComments < ActiveRecord::Migration
  def change
    remove_index :comments, name: "index_comments_on_user_id"
    remove_column :comments, :commenter_id
    remove_column :comments, :commenter_type

    add_column :comments, :user_id, :integer
    add_column :comments, :page_id, :integer
    add_index :comments, :page_id
  end
end

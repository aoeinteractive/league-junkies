class Revertbackallthatshit < ActiveRecord::Migration
  def change
    add_column :posts, :poster_id, :integer
    add_column :posts, :poster_type, :string
    add_index :posts, [:poster_id, :poster_type]
    remove_index :posts, :page_id
    remove_column :posts, :page_id

    add_column :comments, :commenter_id, :integer
    add_column :comments, :commenter_type, :string
    add_index :comments, [:commenter_id, :commenter_type]
    remove_index :comments, :page_id
    remove_column :comments, :page_id
  end
end

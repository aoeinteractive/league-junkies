class AddRootToPosts < ActiveRecord::Migration
  def change
    add_column :posts, :root_id, :integer
    add_index :posts, :root_id
  end
end

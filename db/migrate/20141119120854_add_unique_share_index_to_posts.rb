class AddUniqueShareIndexToPosts < ActiveRecord::Migration
  def change
    add_index :posts, [:poster_id, :poster_type, :root_id], unique: true
  end
end

class RemoveUsernameFromusers < ActiveRecord::Migration
  def change
    remove_index :usernames, :name
    add_index :usernames, :name, unique: true
  end
end

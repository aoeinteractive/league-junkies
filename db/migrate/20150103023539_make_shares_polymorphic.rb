class MakeSharesPolymorphic < ActiveRecord::Migration
  def change
    remove_index :posts, ["poster_id", "poster_type", "root_id"]
    rename_column :posts, :root_id, :shareable_id
    add_column :posts, :shareable_type, :string
    add_index :posts, [:poster_id, :poster_type, :shareable_id, :shareable_type], name: "index_on_shares_unique", unique: true
  end
end

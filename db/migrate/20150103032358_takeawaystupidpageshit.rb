class Takeawaystupidpageshit < ActiveRecord::Migration
  def change
    #comments
    remove_index :comments, :name => "index_comments_on_commenter_id_and_commenter_type"
    remove_column :comments, :commenter_type
    remove_column :comments, :commenter_id

    #favorites
    remove_index :favorites, :name => "index_favorites_on_favoriter_type_and_favoriter_id"
    remove_index :favorites, :name => "favorites_unique_index"
    remove_column :favorites, :favoriter_type
    remove_column :favorites, :favoriter_id
    add_index :favorites, [:favoritable_type, :favoritable_id, :user_id], unique: true, name: "index_favorites_unique"

    #likes
    remove_index :likes, :name => "index_likes_on_liker_type_and_liker_id"
    remove_index :likes, :name => "likes_unique_index"
    add_index :likes, [:likeable_type, :likeable_id, :user_id], unique: true, name: "index_likes_unique"
    remove_column :likes, :liker_type
    remove_column :likes, :liker_id

    #posts
    remove_index :posts, :name => "index_posts_on_poster_id_and_poster_type"
    remove_index :posts, :name => "index_on_shares_unique"
    add_index :posts, [:shareable_id, :shareable_type, :user_id], unique: true, name: "index_shares_unique"
    remove_column :posts, :poster_id
    remove_column :posts, :poster_type

    #mentions
    remove_index :mentions, :name => "index_mentions_on_mentionable_id_and_mentionable_type"
    remove_column :mentions, :mentionable_id
    remove_column :mentions, :mentionable_type
    add_column :mentions, :user_id, :integer
    add_index :mentions, [:user_id]
    add_index :mentions, [:mentioner_id, :mentioner_type, :user_id], name: "index_mentions_unique"
  end
end

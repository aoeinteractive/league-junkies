class RemoveUsernameTable < ActiveRecord::Migration
  def change
    drop_table :usernames
    add_column :users, :username, :string
    add_index :users, :username, unique: true
  end
end

class AddSnippetToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :snippet, :text
  end
end

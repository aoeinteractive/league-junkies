class AddRandomStringToPhotos < ActiveRecord::Migration
  def change
    add_column :photos, :random_string, :string
  end
end

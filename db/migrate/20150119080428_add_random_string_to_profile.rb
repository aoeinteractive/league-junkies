class AddRandomStringToProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :random_string, :string
  end
end

class CreateRiotFeeds < ActiveRecord::Migration
  def change
    create_table :riot_feeds do |t|
      t.string :title
      t.string :image
      t.text :description
      t.string :full_url
      t.string :domain
      t.integer :region_id

      t.timestamps
    end
    add_index :riot_feeds, :title
    add_index :riot_feeds, :region_id
  end
end

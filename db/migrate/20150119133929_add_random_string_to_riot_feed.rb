class AddRandomStringToRiotFeed < ActiveRecord::Migration
  def change
    add_column :riot_feeds, :random_string, :string
  end
end

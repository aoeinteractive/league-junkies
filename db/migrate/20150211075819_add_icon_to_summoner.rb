class AddIconToSummoner < ActiveRecord::Migration
  def change
    add_column :summoners, :icon, :string
  end
end

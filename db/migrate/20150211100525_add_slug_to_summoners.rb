class AddSlugToSummoners < ActiveRecord::Migration
  def change
    add_column :summoners, :slug, :string
    add_index :summoners, :slug, :unique => true
  end
end

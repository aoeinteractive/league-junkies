class CreateSummonerStats < ActiveRecord::Migration
  def change
    create_table :summoner_stats do |t|
      t.integer :summoner_id
      t.string :season
      t.string :game_type
      t.integer :wins
      t.integer :losses
      t.integer :minion_kills
      t.integer :neutral_minion_kills
      t.integer :turret_kills
      t.integer :champion_kills
      t.integer :assists
      t.timestamps
    end

    add_index :summoner_stats, :season
    add_index :summoner_stats, :game_type
    add_index :summoner_stats, :summoner_id
  end
end

class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.integer :riot_id
      t.string :game_mode
      t.string :game_type
      t.string :game_sub_type
      t.integer :map_id
      t.integer :region_id
      t.string :season
      t.timestamps
    end
    add_index :matches, :riot_id
    add_index :matches, :game_mode
    add_index :matches, :game_sub_type
    add_index :matches, :region_id
  end
end

class CreateMatchStats < ActiveRecord::Migration
  def change
    create_table :match_stats do |t|
      t.integer :match_id
      t.integer :summoner_id
      t.integer :level
      t.integer :total_damage_to_champions
      t.integer :gold_earned
      t.integer :wards_placed
      t.integer :wards_killed
      t.integer :total_damage_taken
      t.integer :true_damage_dealt_player
      t.integer :physical_damage_dealt_player
      t.integer :magic_damage_dealt_player
      t.integer :true_damage_to_champions
      t.integer :magic_damage_to_champions
      t.integer :physical_damage_to_champions
      t.integer :magic_damage_taken
      t.integer :physical_damage_taken
      t.integer :true_damage_taken
      t.integer :killing_sprees
      t.integer :largest_crit
      t.integer :total_units_healed
      t.integer :double_kills
      t.integer :triple_kills
      t.integer :quadra_kills
      t.integer :penta_kills
      t.integer :own_neutral_minions_killed
      t.integer :enemy_neutral_minions_killed
      t.integer :turrets_killed
      t.integer :assists
      t.integer :deaths
      t.integer :kills
      t.integer :crowd_control_dealt
      t.integer :largest_multi_kill
      t.integer :total_damage_dealt
      t.integer :largest_killing_spree
      t.integer :total_heal
      t.integer :minions_killed
      t.integer :time_played
      t.integer :gold_spent
      t.integer :neutral_minions_killed
      t.integer :barracks_killed
      t.integer :sight_wards_bought
      t.integer :vision_wards_bought
      t.timestamps
    end
    add_index :match_stats, :match_id
    add_index :match_stats, :summoner_id
  end
end

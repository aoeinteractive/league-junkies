class CreateMatchSummoners < ActiveRecord::Migration
  def change
    create_table :match_summoners do |t|
      t.integer :match_id
      t.integer :summoner_id
      t.integer :ip_earned
      t.integer :level
      t.integer :spell1_id
      t.integer :spell2_id
      t.integer :item0_id
      t.integer :item1_id
      t.integer :item2_id
      t.integer :item3_id
      t.integer :item4_id
      t.integer :item5_id
      t.integer :item6_id
      t.integer :champion_id
      t.integer :map_side
      t.boolean :win
      t.timestamps
    end
    add_index :match_summoners, :summoner_id
    add_index :match_summoners, :match_id
    add_index :match_summoners, :champion_id
    add_index :match_summoners, :win
  end
end

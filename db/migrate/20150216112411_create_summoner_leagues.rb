class CreateSummonerLeagues < ActiveRecord::Migration
  def change
    create_table :summoner_leagues do |t|
      t.integer :summoner_id
      t.string :queue
      t.string :tier
      t.string :division
      t.string :name
      t.integer :points
      t.boolean :fresh_blood
      t.boolean :hot_streak
      t.boolean :inactive
      t.boolean :veteran
      t.string :player_or_team_name
      t.string :player_or_team_id
      t.timestamps
    end

    add_index :summoner_leagues, :summoner_id
    add_index :summoner_leagues, :tier
    add_index :summoner_leagues, :queue
  end
end

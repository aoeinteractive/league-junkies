class CreateInvitations < ActiveRecord::Migration
  def change
    create_table :invitations do |t|
      t.string :email
      t.string :username
      t.string :first_name
      t.string :last_name
      t.datetime :invitation_sent_at
      t.timestamps
    end

    add_index :invitations, :email
    add_index :invitations, :username
    add_index :invitations, :invitation_sent_at
  end
end

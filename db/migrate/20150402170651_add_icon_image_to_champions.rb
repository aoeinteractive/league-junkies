class AddIconImageToChampions < ActiveRecord::Migration
  def change
    add_column :champions, :icon_image, :string
  end
end

class CreateChampionSummoners < ActiveRecord::Migration
  def change
    create_table :champion_summoners do |t|
      t.integer :champion_id
      t.integer :summoner_id
      t.integer :total_deaths
      t.integer :total_played
      t.integer :total_damage_taken
      t.integer :total_quadra_kills
      t.integer :total_triple_kills
      t.integer :total_minion_kills
      t.integer :max_champions_killed
      t.integer :total_double_kills
      t.integer :total_physical_damage_dealt
      t.integer :champion_kills
      t.integer :assists
      t.integer :most_champions_killed
      t.integer :total_damage_dealt
      t.integer :total_first_blood
      t.integer :losses
      t.integer :wins
      t.integer :total_magic_damage_dealt
      t.integer :total_gold_earned
      t.integer :total_penta_kills
      t.integer :total_turrets_killed
      t.integer :max_spells_cast
      t.integer :total_deaths
      t.integer :total_unreal_kills
      t.timestamps
    end
    add_index :champion_summoners, :champion_id
    add_index :champion_summoners, :summoner_id
  end
end

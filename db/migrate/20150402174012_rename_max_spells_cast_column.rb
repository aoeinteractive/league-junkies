class RenameMaxSpellsCastColumn < ActiveRecord::Migration
  def change
    rename_column :champion_summoners, :max_spells_cast, :most_spells_cast
  end
end

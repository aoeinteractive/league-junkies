class ChangeRiotIdTypeInItems < ActiveRecord::Migration
  def change
    remove_column :items, :riot_id
    add_column :items, :riot_id, :integer
    add_index :items, :riot_id
  end
end

class AddPercentageToChampionSummoners < ActiveRecord::Migration
  def change
    add_column :champion_summoners, :percentage, :decimal, precision: 6, scale: 5
    add_index :champion_summoners, :percentage
  end
end

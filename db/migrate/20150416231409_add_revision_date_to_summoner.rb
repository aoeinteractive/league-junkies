class AddRevisionDateToSummoner < ActiveRecord::Migration
  def change
    add_column :summoners, :revision_date, :datetime
  end
end

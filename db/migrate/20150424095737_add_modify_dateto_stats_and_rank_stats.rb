class AddModifyDatetoStatsAndRankStats < ActiveRecord::Migration
  def change
    add_column :summoner_stats, :modify_date, :datetime
  end
end

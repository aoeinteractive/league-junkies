class ChangeRiotIdType < ActiveRecord::Migration
  def change
    change_column :matches, :riot_id, :bigint
    change_column :summoners, :riot_id, :bigint
  end
end

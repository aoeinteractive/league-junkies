class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :name
      t.integer :host_id
      t.string :host_type
      t.string :image
      t.text :description
      t.datetime :start
      t.datetime :end
      t.boolean :hidden
      t.string :category
      t.timestamps
    end
    add_index :events, [:host_id, :host_type]
    add_index :events, :hidden
    add_index :events, :category
  end
end

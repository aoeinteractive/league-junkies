class AddRandomStringToEvents < ActiveRecord::Migration
  def change
    add_column :events, :random_string, :string
  end
end

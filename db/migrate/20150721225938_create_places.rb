class CreatePlaces < ActiveRecord::Migration
  def change
    create_table :places do |t|
      t.string :name
      t.string :formatted_address
      t.text :html_text
      t.string :image
      t.string :formatted_phone_number
      t.float :rating
      t.string :website
      t.string :street
      t.string :city
      t.string :state
      t.string :country
      t.string :postal_code
      t.float :longitude
      t.float :latitude
      t.string :google_place_id
      t.timestamps
    end
  end
end

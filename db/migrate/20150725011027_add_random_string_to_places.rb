class AddRandomStringToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :random_string, :string
  end
end

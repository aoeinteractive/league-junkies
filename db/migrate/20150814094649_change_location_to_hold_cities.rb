class ChangeLocationToHoldCities < ActiveRecord::Migration
  def change
    remove_column :locations, :address
    remove_column :locations, :locatable_id
    remove_column :locations, :locatable_type

    add_column :profiles, :location_id, :integer
    add_index :profiles, :location_id
    add_column :posts, :location_id, :integer
    add_index :posts, :location_id
  end
end
      
class AddSlugsToEventsAndPlaces < ActiveRecord::Migration
  def change
    add_column :events, :slug, :string
    add_column :places, :slug, :string
    add_index :events, :slug, unique: true
    add_index :places, :slug, unique: true
  end
end

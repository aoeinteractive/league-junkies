class AddRouteToPlaces < ActiveRecord::Migration
  def change
    add_column :places, :route, :string
    rename_column :places, :street, :street_number
  end
end

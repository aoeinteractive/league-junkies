class RemoveHtmlDescFromPlaces < ActiveRecord::Migration
  def change
    remove_column :places, :html_text
  end
end

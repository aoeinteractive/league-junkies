class AddCounterColumnsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :rsvps_count, :integer, default: 0
    add_column :events, :goings_count, :integer, default: 0
    add_column :events, :maybes_count, :integer, default: 0
    add_column :events, :declines_count, :integer, default: 0
  end
end

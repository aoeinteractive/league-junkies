class AddPopularFunctionToEvents < ActiveRecord::Migration
  def up
    execute <<-SQL
        create or replace function
          popularity(count integer, weight integer default 3) RETURNS integer AS $$ SELECT count * weight $$ LANGUAGE SQL IMMUTABLE;
    SQL
  end

  def down
    exetute "DROP FUNCTION IF EXISTS popularity(integer, integer);"
  end
end

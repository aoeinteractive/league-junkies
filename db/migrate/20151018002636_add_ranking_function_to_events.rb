class AddRankingFunctionToEvents < ActiveRecord::Migration
  def up
    execute <<-SQL
      create or replace function
        ranking(id integer, counts integer, weight integer) RETURNS integer AS $$ SELECT id + popularity(counts, weight) $$ LANGUAGE SQL IMMUTABLE;
    SQL
  end

  def down
    execute "DROP FUNCTION IF EXISTS ranking(integer, timestamp, integer);"
  end
end

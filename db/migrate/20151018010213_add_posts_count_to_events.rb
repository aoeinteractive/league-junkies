class AddPostsCountToEvents < ActiveRecord::Migration
  def change
    add_column :events, :posts_count, :integer, default: 0
  end
end

class AddCountsIndexes < ActiveRecord::Migration
  def change
    add_index :events, :rsvps_count
    add_index :events, :goings_count
    add_index :events, :maybes_count
    add_index :events, :declines_count
    add_index :events, :posts_count
  end
end

class AddIndexToRankingEvents < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE INDEX index_events_on_ranking
      ON events (ranking(events.id, rsvps_count + posts_count, 3) DESC);   
    SQL
  end
end

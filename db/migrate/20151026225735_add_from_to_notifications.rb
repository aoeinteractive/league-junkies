class AddFromToNotifications < ActiveRecord::Migration
  def change
    add_column :notifications, :notifier_id, :integer
    add_column :notifications, :notifier_type, :string
    add_index :notifications, [:notifier_id, :notifier_type]
  end
end

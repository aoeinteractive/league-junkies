class DeleteMessageNotifications < ActiveRecord::Migration
  def change
    Notification.where(notifiable_type: "Mailboxer::Notification").destroy_all
  end
end

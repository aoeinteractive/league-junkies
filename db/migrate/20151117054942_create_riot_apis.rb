class CreateRiotApis < ActiveRecord::Migration
  def change
    create_table :riot_apis do |t|
      t.string :name
      t.string :version
      t.timestamps
    end
    add_index :riot_apis, :name
  end
end

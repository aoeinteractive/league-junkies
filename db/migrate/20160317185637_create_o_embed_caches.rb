class CreateOEmbedCaches < ActiveRecord::Migration
  def change
    create_table :o_embed_caches do |t|
      t.string :url
      t.text :data

      t.timestamps
    end
    add_index :o_embed_caches, :url
  end
end

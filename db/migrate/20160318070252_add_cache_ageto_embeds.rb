class AddCacheAgetoEmbeds < ActiveRecord::Migration
  def change
    add_column :o_embed_caches, :cache_age, :datetime
    add_index :o_embed_caches, :cache_age
  end
end

class AddCounterToHashtags < ActiveRecord::Migration
  def change
    add_column :simple_hashtag_hashtags, :posts_count, :integer
    add_index :simple_hashtag_hashtags, :posts_count
  end
end

class AddIndexToHashtagName < ActiveRecord::Migration
  def change
    add_index :simple_hashtag_hashtags, :name
  end
end

class AddDisplayToHashtag < ActiveRecord::Migration
  def change
    add_column :simple_hashtag_hashtags, :display, :string
  end
end

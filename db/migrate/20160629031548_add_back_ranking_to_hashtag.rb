class AddBackRankingToHashtag < ActiveRecord::Migration
  def change
    add_column :simple_hashtag_hashtags, :last_post_id, :integer
    add_index :simple_hashtag_hashtags, :last_post_id  
  end
end

class AddLastPostAtToHashtags < ActiveRecord::Migration
  def change
    add_column :simple_hashtag_hashtags, :last_post_at, :datetime
  end
end

class ChangeHashtagColumnsToDefault < ActiveRecord::Migration
  def change
    change_column :simple_hashtag_hashtags, :posts_count, :integer, default: 0
    change_column :simple_hashtag_hashtags, :last_post_id, :integer, default: 0
  end
end

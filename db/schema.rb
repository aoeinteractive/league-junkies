# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20160703225354) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "champion_summoners", :force => true do |t|
    t.integer  "champion_id"
    t.integer  "summoner_id"
    t.integer  "total_deaths"
    t.integer  "total_played"
    t.integer  "total_damage_taken"
    t.integer  "total_quadra_kills"
    t.integer  "total_triple_kills"
    t.integer  "total_minion_kills"
    t.integer  "max_champions_killed"
    t.integer  "total_double_kills"
    t.integer  "total_physical_damage_dealt"
    t.integer  "champion_kills"
    t.integer  "assists"
    t.integer  "most_champions_killed"
    t.integer  "total_damage_dealt"
    t.integer  "total_first_blood"
    t.integer  "losses"
    t.integer  "wins"
    t.integer  "total_magic_damage_dealt"
    t.integer  "total_gold_earned"
    t.integer  "total_penta_kills"
    t.integer  "total_turrets_killed"
    t.integer  "most_spells_cast"
    t.integer  "total_unreal_kills"
    t.datetime "created_at",                                                :null => false
    t.datetime "updated_at",                                                :null => false
    t.decimal  "percentage",                  :precision => 6, :scale => 5
  end

  add_index "champion_summoners", ["champion_id"], :name => "index_champion_summoners_on_champion_id"
  add_index "champion_summoners", ["percentage"], :name => "index_champion_summoners_on_percentage"
  add_index "champion_summoners", ["summoner_id"], :name => "index_champion_summoners_on_summoner_id"

  create_table "champions", :force => true do |t|
    t.integer  "riot_id"
    t.string   "name"
    t.text     "tags"
    t.text     "stats"
    t.string   "splash_image"
    t.string   "loading_image"
    t.string   "title"
    t.text     "lore"
    t.text     "rating"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "icon_image"
  end

  create_table "comments", :force => true do |t|
    t.string   "commentable_type"
    t.integer  "commentable_id"
    t.text     "body"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.integer  "likes_count",      :default => 0
    t.integer  "user_id"
  end

  add_index "comments", ["commentable_id", "commentable_type"], :name => "index_comments_on_commentable_id_and_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "events", :force => true do |t|
    t.string   "name"
    t.integer  "host_id"
    t.string   "host_type"
    t.string   "image"
    t.text     "description"
    t.datetime "start"
    t.datetime "end"
    t.boolean  "hidden"
    t.string   "category"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.string   "random_string"
    t.string   "slug"
    t.integer  "place_id"
    t.integer  "rsvps_count",    :default => 0
    t.integer  "goings_count",   :default => 0
    t.integer  "maybes_count",   :default => 0
    t.integer  "declines_count", :default => 0
    t.integer  "posts_count",    :default => 0
  end

  add_index "events", ["category"], :name => "index_events_on_category"
  add_index "events", ["declines_count"], :name => "index_events_on_declines_count"
  add_index "events", ["goings_count"], :name => "index_events_on_goings_count"
  add_index "events", ["hidden"], :name => "index_events_on_hidden"
  add_index "events", ["host_id", "host_type"], :name => "index_events_on_host_id_and_host_type"
  add_index "events", ["maybes_count"], :name => "index_events_on_maybes_count"
  add_index "events", ["place_id"], :name => "index_events_on_place_id"
  add_index "events", ["posts_count"], :name => "index_events_on_posts_count"
  add_index "events", ["rsvps_count"], :name => "index_events_on_rsvps_count"
  add_index "events", ["slug"], :name => "index_events_on_slug", :unique => true

  create_table "favorites", :force => true do |t|
    t.integer  "favoritable_id"
    t.string   "favoritable_type"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "user_id"
  end

  add_index "favorites", ["favoritable_type", "favoritable_id", "user_id"], :name => "index_favorites_unique", :unique => true
  add_index "favorites", ["user_id"], :name => "index_favorites_on_user_id"

  create_table "follows", :force => true do |t|
    t.integer  "followable_id",                      :null => false
    t.string   "followable_type",                    :null => false
    t.integer  "follower_id",                        :null => false
    t.string   "follower_type",                      :null => false
    t.boolean  "blocked",         :default => false, :null => false
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
  end

  add_index "follows", ["followable_id", "followable_type"], :name => "fk_followables"
  add_index "follows", ["follower_id", "follower_type"], :name => "fk_follows"

  create_table "genders", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "invitations", :force => true do |t|
    t.string   "email"
    t.string   "username"
    t.string   "first_name"
    t.string   "last_name"
    t.datetime "invitation_sent_at"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  add_index "invitations", ["email"], :name => "index_invitations_on_email"
  add_index "invitations", ["invitation_sent_at"], :name => "index_invitations_on_invitation_sent_at"
  add_index "invitations", ["username"], :name => "index_invitations_on_username"

  create_table "items", :force => true do |t|
    t.string   "name"
    t.text     "description"
    t.text     "tags"
    t.string   "image"
    t.text     "from"
    t.text     "into"
    t.integer  "depth"
    t.integer  "cost"
    t.integer  "sell"
    t.integer  "base"
    t.boolean  "purchasable"
    t.string   "group"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "riot_id"
  end

  add_index "items", ["riot_id"], :name => "index_items_on_riot_id"

  create_table "likes", :force => true do |t|
    t.string   "likeable_type"
    t.integer  "likeable_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "user_id"
  end

  add_index "likes", ["likeable_type", "likeable_id", "user_id"], :name => "index_likes_unique", :unique => true
  add_index "likes", ["likeable_type", "likeable_id"], :name => "index_likes_on_likeable_type_and_likeable_id"
  add_index "likes", ["user_id"], :name => "index_likes_on_user_id"

  create_table "locations", :force => true do |t|
    t.string   "state"
    t.string   "city"
    t.string   "country"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.string   "formatted_address"
  end

  create_table "mailboxer_conversation_opt_outs", :force => true do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], :name => "index_mailboxer_conversation_opt_outs_on_conversation_id"
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], :name => "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type"

  create_table "mailboxer_conversations", :force => true do |t|
    t.string   "subject",    :default => ""
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
  end

  create_table "mailboxer_notifications", :force => true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              :default => ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                :default => false
    t.string   "notification_code"
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "attachment"
    t.datetime "updated_at",                              :null => false
    t.datetime "created_at",                              :null => false
    t.boolean  "global",               :default => false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], :name => "index_mailboxer_notifications_on_conversation_id"
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], :name => "index_mailboxer_notifications_on_notified_object_id_and_type"
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], :name => "index_mailboxer_notifications_on_sender_id_and_sender_type"
  add_index "mailboxer_notifications", ["type"], :name => "index_mailboxer_notifications_on_type"

  create_table "mailboxer_receipts", :force => true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                                  :null => false
    t.boolean  "is_read",                       :default => false
    t.boolean  "trashed",                       :default => false
    t.boolean  "deleted",                       :default => false
    t.string   "mailbox_type",    :limit => 25
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
  end

  add_index "mailboxer_receipts", ["notification_id"], :name => "index_mailboxer_receipts_on_notification_id"
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], :name => "index_mailboxer_receipts_on_receiver_id_and_receiver_type"

  create_table "match_stats", :force => true do |t|
    t.integer  "match_id"
    t.integer  "summoner_id"
    t.integer  "level"
    t.integer  "total_damage_to_champions"
    t.integer  "gold_earned"
    t.integer  "wards_placed"
    t.integer  "wards_killed"
    t.integer  "total_damage_taken"
    t.integer  "true_damage_dealt_player"
    t.integer  "physical_damage_dealt_player"
    t.integer  "magic_damage_dealt_player"
    t.integer  "true_damage_to_champions"
    t.integer  "magic_damage_to_champions"
    t.integer  "physical_damage_to_champions"
    t.integer  "magic_damage_taken"
    t.integer  "physical_damage_taken"
    t.integer  "true_damage_taken"
    t.integer  "killing_sprees"
    t.integer  "largest_crit"
    t.integer  "total_units_healed"
    t.integer  "double_kills"
    t.integer  "triple_kills"
    t.integer  "quadra_kills"
    t.integer  "penta_kills"
    t.integer  "own_neutral_minions_killed"
    t.integer  "enemy_neutral_minions_killed"
    t.integer  "turrets_killed"
    t.integer  "assists"
    t.integer  "deaths"
    t.integer  "kills"
    t.integer  "crowd_control_dealt"
    t.integer  "largest_multi_kill"
    t.integer  "total_damage_dealt"
    t.integer  "largest_killing_spree"
    t.integer  "total_heal"
    t.integer  "minions_killed"
    t.integer  "time_played"
    t.integer  "gold_spent"
    t.integer  "neutral_minions_killed"
    t.integer  "barracks_killed"
    t.integer  "sight_wards_bought"
    t.integer  "vision_wards_bought"
    t.datetime "created_at",                   :null => false
    t.datetime "updated_at",                   :null => false
  end

  add_index "match_stats", ["match_id"], :name => "index_match_stats_on_match_id"
  add_index "match_stats", ["summoner_id"], :name => "index_match_stats_on_summoner_id"

  create_table "match_summoners", :force => true do |t|
    t.integer  "match_id"
    t.integer  "summoner_id"
    t.integer  "ip_earned"
    t.integer  "level"
    t.integer  "spell1_id"
    t.integer  "spell2_id"
    t.integer  "item0_id"
    t.integer  "item1_id"
    t.integer  "item2_id"
    t.integer  "item3_id"
    t.integer  "item4_id"
    t.integer  "item5_id"
    t.integer  "item6_id"
    t.integer  "champion_id"
    t.integer  "map_side"
    t.boolean  "win"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "match_summoners", ["champion_id"], :name => "index_match_summoners_on_champion_id"
  add_index "match_summoners", ["match_id"], :name => "index_match_summoners_on_match_id"
  add_index "match_summoners", ["summoner_id"], :name => "index_match_summoners_on_summoner_id"
  add_index "match_summoners", ["win"], :name => "index_match_summoners_on_win"

  create_table "matches", :force => true do |t|
    t.integer  "riot_id",       :limit => 8
    t.string   "game_mode"
    t.string   "game_type"
    t.string   "game_sub_type"
    t.integer  "map_id"
    t.integer  "region_id"
    t.string   "season"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.datetime "riot_date"
  end

  add_index "matches", ["game_mode"], :name => "index_matches_on_game_mode"
  add_index "matches", ["game_sub_type"], :name => "index_matches_on_game_sub_type"
  add_index "matches", ["region_id"], :name => "index_matches_on_region_id"
  add_index "matches", ["riot_id"], :name => "index_matches_on_riot_id"

  create_table "maxmind_geolite_city_blocks", :id => false, :force => true do |t|
    t.integer "start_ip_num", :limit => 8, :null => false
    t.integer "end_ip_num",   :limit => 8, :null => false
    t.integer "loc_id",       :limit => 8, :null => false
  end

  add_index "maxmind_geolite_city_blocks", ["end_ip_num", "start_ip_num"], :name => "index_maxmind_geolite_city_blocks_on_end_ip_num_range", :unique => true
  add_index "maxmind_geolite_city_blocks", ["loc_id"], :name => "index_maxmind_geolite_city_blocks_on_loc_id"
  add_index "maxmind_geolite_city_blocks", ["start_ip_num"], :name => "index_maxmind_geolite_city_blocks_on_start_ip_num", :unique => true

  create_table "maxmind_geolite_city_location", :id => false, :force => true do |t|
    t.integer "loc_id",      :limit => 8, :null => false
    t.string  "country",                  :null => false
    t.string  "region",                   :null => false
    t.string  "city"
    t.string  "postal_code",              :null => false
    t.float   "latitude"
    t.float   "longitude"
    t.integer "metro_code"
    t.integer "area_code"
  end

  add_index "maxmind_geolite_city_location", ["loc_id"], :name => "index_maxmind_geolite_city_location_on_loc_id", :unique => true

  create_table "mentions", :force => true do |t|
    t.string   "mentioner_type"
    t.integer  "mentioner_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "user_id"
  end

  add_index "mentions", ["mentioner_id", "mentioner_type", "user_id"], :name => "index_mentions_unique"
  add_index "mentions", ["mentioner_id", "mentioner_type"], :name => "index_mentions_on_mentioner_id_and_mentioner_type"
  add_index "mentions", ["user_id"], :name => "index_mentions_on_user_id"

  create_table "notifications", :force => true do |t|
    t.integer  "user_id"
    t.boolean  "read",            :default => false
    t.integer  "notifiable_id"
    t.string   "notifiable_type"
    t.datetime "created_at",                         :null => false
    t.datetime "updated_at",                         :null => false
    t.integer  "notifier_id"
    t.string   "notifier_type"
  end

  add_index "notifications", ["notifiable_id", "notifiable_type"], :name => "index_notifications_on_notifiable_id_and_notifiable_type"
  add_index "notifications", ["notifier_id", "notifier_type"], :name => "index_notifications_on_notifier_id_and_notifier_type"
  add_index "notifications", ["read"], :name => "index_notifications_on_read"
  add_index "notifications", ["user_id"], :name => "index_notifications_on_user_id"

  create_table "o_embed_caches", :force => true do |t|
    t.string   "url"
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.datetime "cache_age"
  end

  add_index "o_embed_caches", ["cache_age"], :name => "index_o_embed_caches_on_cache_age"
  add_index "o_embed_caches", ["url"], :name => "index_o_embed_caches_on_url"

  create_table "photos", :force => true do |t|
    t.integer  "user_id"
    t.integer  "photoable_id"
    t.string   "photoable_type"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",                    :null => false
    t.datetime "updated_at",                    :null => false
    t.integer  "width"
    t.integer  "height"
    t.string   "random_string"
    t.integer  "likes_count",    :default => 0
    t.integer  "comments_count", :default => 0
  end

  add_index "photos", ["photoable_id", "photoable_type"], :name => "index_photos_on_photoable_id_and_photoable_type"
  add_index "photos", ["user_id"], :name => "index_photos_on_user_id"

  create_table "places", :force => true do |t|
    t.string   "name"
    t.string   "formatted_address"
    t.string   "image"
    t.string   "formatted_phone_number"
    t.float    "rating"
    t.string   "website"
    t.string   "street_number"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "postal_code"
    t.float    "longitude"
    t.float    "latitude"
    t.string   "google_place_id"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "random_string"
    t.string   "slug"
    t.string   "route"
    t.string   "type"
  end

  add_index "places", ["slug"], :name => "index_places_on_slug", :unique => true

  create_table "posts", :force => true do |t|
    t.text     "body"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.string   "type"
    t.integer  "shares_count",     :default => 0
    t.integer  "likes_count",      :default => 0
    t.integer  "favorites_count",  :default => 0
    t.integer  "comments_count",   :default => 0
    t.integer  "user_id"
    t.integer  "shareable_id"
    t.string   "shareable_type"
    t.integer  "postable_id"
    t.string   "postable_type"
    t.integer  "location_id"
    t.integer  "o_embed_cache_id"
  end

  add_index "posts", ["location_id"], :name => "index_posts_on_location_id"
  add_index "posts", ["postable_id", "postable_type"], :name => "index_posts_on_postable_id_and_postable_type"
  add_index "posts", ["shareable_id", "shareable_type", "user_id"], :name => "index_shares_unique", :unique => true
  add_index "posts", ["shareable_id"], :name => "index_posts_on_root_id"
  add_index "posts", ["user_id"], :name => "index_posts_on_user_id"

  create_table "profiles", :force => true do |t|
    t.integer  "user_id"
    t.date     "birthday"
    t.string   "gender"
    t.text     "about"
    t.text     "websites"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "first_name"
    t.string   "last_name"
    t.text     "snippet"
    t.string   "image"
    t.string   "random_string"
    t.integer  "location_id"
  end

  add_index "profiles", ["location_id"], :name => "index_profiles_on_location_id"

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.string   "code"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "riot_apis", :force => true do |t|
    t.string   "name"
    t.string   "version"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "riot_apis", ["name"], :name => "index_riot_apis_on_name"

  create_table "riot_feeds", :force => true do |t|
    t.string   "title"
    t.string   "image"
    t.text     "description"
    t.string   "full_url"
    t.string   "domain"
    t.integer  "region_id"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "random_string"
  end

  add_index "riot_feeds", ["region_id"], :name => "index_riot_feeds_on_region_id"
  add_index "riot_feeds", ["title"], :name => "index_riot_feeds_on_title"

  create_table "rsvps", :force => true do |t|
    t.integer  "user_id"
    t.integer  "rsvpable_id"
    t.string   "rsvpable_type"
    t.string   "status"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "rsvps", ["rsvpable_id", "rsvpable_type"], :name => "index_rsvps_on_rsvpable_id_and_rsvpable_type"
  add_index "rsvps", ["status"], :name => "index_rsvps_on_status"
  add_index "rsvps", ["user_id"], :name => "index_rsvps_on_user_id"

  create_table "simple_hashtag_hashtaggings", :force => true do |t|
    t.integer "hashtag_id"
    t.integer "hashtaggable_id"
    t.string  "hashtaggable_type"
  end

  add_index "simple_hashtag_hashtaggings", ["hashtaggable_id", "hashtaggable_type"], :name => "index_hashtaggings_hashtaggable_id_hashtaggable_type"

  create_table "simple_hashtag_hashtags", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",                  :null => false
    t.datetime "updated_at",                  :null => false
    t.integer  "posts_count",  :default => 0
    t.string   "display"
    t.integer  "last_post_id", :default => 0
    t.datetime "last_post_at"
  end

  add_index "simple_hashtag_hashtags", ["last_post_id"], :name => "index_simple_hashtag_hashtags_on_last_post_id"
  add_index "simple_hashtag_hashtags", ["name"], :name => "index_simple_hashtag_hashtags_on_name"
  add_index "simple_hashtag_hashtags", ["posts_count"], :name => "index_simple_hashtag_hashtags_on_posts_count"

  create_table "spells", :force => true do |t|
    t.integer  "riot_id"
    t.string   "name"
    t.integer  "level"
    t.text     "description"
    t.string   "image"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "summoner_leagues", :force => true do |t|
    t.integer  "summoner_id"
    t.string   "queue"
    t.string   "tier"
    t.string   "division"
    t.string   "name"
    t.integer  "points"
    t.boolean  "fresh_blood"
    t.boolean  "hot_streak"
    t.boolean  "inactive"
    t.boolean  "veteran"
    t.string   "player_or_team_name"
    t.string   "player_or_team_id"
    t.datetime "created_at",          :null => false
    t.datetime "updated_at",          :null => false
  end

  add_index "summoner_leagues", ["queue"], :name => "index_summoner_leagues_on_queue"
  add_index "summoner_leagues", ["summoner_id"], :name => "index_summoner_leagues_on_summoner_id"
  add_index "summoner_leagues", ["tier"], :name => "index_summoner_leagues_on_tier"

  create_table "summoner_stats", :force => true do |t|
    t.integer  "summoner_id"
    t.string   "season"
    t.string   "game_type"
    t.integer  "wins"
    t.integer  "losses"
    t.integer  "minion_kills"
    t.integer  "neutral_minion_kills"
    t.integer  "turret_kills"
    t.integer  "champion_kills"
    t.integer  "assists"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.datetime "modify_date"
  end

  add_index "summoner_stats", ["game_type"], :name => "index_summoner_stats_on_game_type"
  add_index "summoner_stats", ["season"], :name => "index_summoner_stats_on_season"
  add_index "summoner_stats", ["summoner_id"], :name => "index_summoner_stats_on_summoner_id"

  create_table "summoners", :force => true do |t|
    t.integer  "riot_id",              :limit => 8
    t.string   "name"
    t.integer  "level"
    t.integer  "region_id"
    t.integer  "user_id"
    t.string   "verification_token"
    t.datetime "verification_sent_at"
    t.datetime "verified_at"
    t.string   "unconfirmed_name"
    t.datetime "created_at",                        :null => false
    t.datetime "updated_at",                        :null => false
    t.string   "icon"
    t.string   "slug"
    t.datetime "revision_date"
  end

  add_index "summoners", ["slug"], :name => "index_summoners_on_slug", :unique => true

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0,  :null => false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        :default => 0,  :null => false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "username"
    t.string   "slug"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit",       :default => 10
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["slug"], :name => "index_users_on_slug", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true
  add_index "users", ["username"], :name => "index_users_on_username", :unique => true

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", name: "mb_opt_outs_on_conversations_id", column: "conversation_id"

  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", name: "notifications_on_conversation_id", column: "conversation_id"

  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", name: "receipts_on_notification_id", column: "notification_id"

end

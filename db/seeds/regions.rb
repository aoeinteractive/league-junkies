Region.create(name: "Brazil", code: "br")
Region.create(name: "Europe Nordic & East", code: "eune")
Region.create(name: "Europe West", code: "euw")
Region.create(name: "North America", code: "na")
Region.create(name: "Korea", code: "kr")
Region.create(name: "Oceania", code: "oce")
Region.create(name: "Turkey", code: "tr")
Region.create(name: "Latin America North", code: "lan")
Region.create(name: "Latin America South", code: "las")
Region.create(name: "Russia", code: "ru")
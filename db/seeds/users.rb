u = User.new(
  email: "info@leaguejunkies.com",
  username: ENV["master_account"],
  password: ENV["master_password"],
  password_confirmation: ENV["master_password"]
)

u.skip_confirmation!
u.save!

Profile.create!(first_name: "League", last_name: "Junkies", user: u)

demo1 = User.new(
  email: "demo1@leaguejunkies.com",
  username: "Demo1",
  password: "d3m054321",
  password_confirmation: "d3m054321"
)

demo1.skip_confirmation!
demo1.save!

Profile.create!(first_name: "Demo", last_name: "Account", user: demo1)
module SimpleHashtagExtension
  extend ActiveSupport::Concern
  included do
    searchkick autocomplete: ['name']
    scope :trending, -> {order("ranking(last_post_id, posts_count, 3) DESC")}
  end
end

SimpleHashtag::Hashtag.send(:include, SimpleHashtagExtension)
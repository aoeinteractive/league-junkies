namespace :champions do
  desc "Fetch Champions from Riot API"
  task create: :environment do
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.champion.get
    datas.each do |data|
      Champion.create(
        riot_id: data.id,
        name: data.name,
        remote_loading_image_url: "http://ddragon.leagueoflegends.com/cdn/img/champion/loading/#{data.key}_0.jpg",
        remote_icon_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/champion/#{data.key}.png"
      )
    end
  end
end
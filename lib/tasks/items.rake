namespace :items do
  desc "Fetch Items from Riot API then get older from DDragons"
  task create: :environment do
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.item.get(itemListData: 'all')
    datas.each do |data|
      Item.create(
        riot_id: data.id,
        name: data.name,
        remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/item/#{data.image['full']}"
      )
    end
  end

  desc "Fetch Older Items from DDragon"
  task create_older: :environment do 
    items_version = RiotApi.original_items.version
    url = "http://ddragon.leagueoflegends.com/cdn/#{items_version}/data/en_US/item.json"
    doc = JSON.load(open(url))
    doc['data'].each do |data|
      Item.where(
        riot_id: data[0],
        name: data[1]['name']
      ).first_or_create(
        remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{items_version}/img/item/#{data[1]['image']['full']}"
      )
    end
  end
end
# geocoder times out sometimes, script also takes sooo long

namespace :db do
  desc "Popular DB with Faker Gem"
  task populate: :environment do

    module SecureFaker
      def image(*args)
        super(*args).sub('http://', 'https://')
      end
    end

    Faker::Avatar.singleton_class.prepend(SecureFaker)

    genders = ["Male", "Female", "Unspecified"]
    hashtags = ["League of Legends", "LCS", "eSports", "baylife", "Razer", "Corsair", "OriginPC", "Alienware"]

    hashtags.each do |hashtag|
      SimpleHashtag::Hashtag.create!(
        name: hashtag
      )
    end

    500.times do |n|
      location = Geocoder.search(Faker::Internet.ip_v4_address).first
      
      # LOCATION
      loc = Location.new(
        state: location.state,
        city: location.city,
        country: location.country,
        latitude: location.latitude,
        longitude: location.longitude,
        formatted_address: location.address
      )
      loc.save!

      # PLACE
      place = Place.new(
        name: Faker::Company.name,
        formatted_address: location.address,
        formatted_phone_number: Faker::PhoneNumber.phone_number,
      website: 'http://' + Faker::Internet.domain_name,
        city: location.city,
        state: location.state,
        country: location.country,
        postal_code: location.postal_code,
        latitude: location.latitude,
        longitude: location.longitude
      )
      place.save!

      # USER
      u = User.new(
        email: Faker::Internet.email(Faker::Name.first_name + "_" + Faker::Name.last_name),
        username: "#{Faker::Internet.user_name}#{n}",
        password: "changeme",
        password_confirmation: "changeme"
      )
      u.skip_confirmation!
      u.save!

      # PROFILE
      Profile.create!(
        first_name: Faker::Name.first_name,
        last_name: Faker::Name.last_name,
        birthday: rand(50*365).days.ago,
        gender: genders.sample,
        about: Faker::Lorem.paragraph,
        snippet: Faker::Lorem.sentence(40),
        remote_image_url: Faker::Avatar.image,
        location: loc,
        user: u 
      )

      # EVENTS
      5.times do 
        u.events.create!(
          name: "#{Faker::University.name} #{Faker::App.name}",
          remote_image_url: "http://static.leaguejunkies.com/images/lj-badge.png",
          description: Faker::Lorem.paragraph,
          start: Faker::Time.between(DateTime.now, 1.week.from_now),
          end: Faker::Time.between(1.week.from_now, 2.weeks.from_now),
          place_id: place.id
        )
      end

      # USER POSTS
      10.times do 
        u.status_messages.create!(
          body: "#{Faker::Lorem.paragraph} ##{hashtags.sample}"
        )
      end
    end

    # EVENT POSTS
    Event.all.each do |event|
      10.times do 
        event.status_messages.create!(user: User.all.sample, body: Faker::Lorem.paragraph)
      end
    end

    # FOLLOW SO BAD LOL
    User.all.each do |u|
      users = User.where("id not in (?)", u.id)
      users.each do |uu|
        u.follow(uu)
      end
    end

  end
end
namespace :riot_news do 
  desc "Fetch Riot News"
  task fetch: :environment do
    base = "http://na.leagueoflegends.com" 
    url = "#{base}/en/news"
    doc = Nokogiri::HTML(open(url))
    reversed_array = doc.css(".node-article").reverse
    reversed_array.each do |news|
      RiotFeed.where(
        title: news.at_css('h4').text,
        description: news.at_css('.teaser-content .field').text,
        domain: URI.parse(base).host,
        full_url: base + news.at_css('a')[:href]
      ).first_or_create(
        remote_image_url: base + news.at_css('img')[:src], region: Region.find_by_code('na')
      )
    end
  end
end
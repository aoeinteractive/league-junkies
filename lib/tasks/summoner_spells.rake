namespace :summoner_spells do
  desc "Fetch Summoner Spells from Riot API"
  task create: :environment do
    patch_version = RiotApi.patch.version
    datas = $riot_na.static.summoner_spell.get(spellData: 'all')
    datas.each do |data|
      Spell.create(
        riot_id: data.id,
        name: data.name,
        description: data.sanitizedDescription,
        level: data.summonerLevel,
        remote_image_url: "http://ddragon.leagueoflegends.com/cdn/#{patch_version}/img/spell/#{data.image['full']}"
      )
    end
  end
end
namespace :users do
  desc "Invite 100 users to sign up"
  task invite: :environment do
    invitations = Invitation.not_invited.limit(100)
    invitations.each do |u|
      u.invitation_sent_at = Time.now.utc
      u.save!
      u_account = User.new(email: u.email, username: u.username)
      u_account.save!
      Profile.create!(first_name: u.first_name, last_name: u.last_name, user: u_account)
    end
  end

  desc "Invite specific user by email"
  task invite_user: :environment do
    email = ENV["invited_email"]
    if invitation = Invitation.not_invited.find_by_email(email)
      invitation.invitation_sent_at = Time.now.utc
      invitation.save!
      account = User.new(email: invitation.email, username: invitation.username)
      account.save!
      Profile.create!(first_name: invitation.first_name, last_name: invitation.last_name, user: account)
    end
  end

  desc "Subscribe current user list to Mailchimp"
  task mailchimp: :environment do
    gb = Gibbon::API.new
    User.find_each do |user|
      gb.lists.subscribe({
        id: ENV["mailchimp_list"], 
        email: {email: user.email},
        double_optin: false,
        merge_vars: {:FNAME => user.profile.first_name, :LNAME => user.profile.last_name}
      })
    end
  end
end
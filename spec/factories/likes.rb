# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :like do
    liker_type "MyString"
    liker_id 1
    likeable_type "MyString"
    likeable_id 1
  end
end

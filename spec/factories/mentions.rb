# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mention do
    mentioner_type "MyString"
    mentioner_id 1
    mentionable_type "MyString"
    mentionable_id 1
  end
end

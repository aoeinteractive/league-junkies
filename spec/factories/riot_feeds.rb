# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :riot_feed do
    title "MyString"
    image "MyString"
    description "MyText"
    full_url "MyString"
    domain "MyString"
    region "MyString"
  end
end
